﻿////function firstPagePostsLoad() {
////    let keyword = "";
////    let data = {
////        keyword: keyword
////    };
////    $.ajax({
////        url: '/Home/IndexPostSearchResults',
////        contentType: "application/json",
////        type: "POST",
////        data: JSON.stringify(data),
////        dataType: 'html'
////    })
////        .done(function (result) {
////            $('#post-result-id').html(result);
////        });
////    //$('#post-result-id').load('/home/indexpostsearchresults', { keyword: keyword });
////}

function searchEventHandler() {
    let keyword = $('#index-post-keyword').val();
    let data = {
        keyword: keyword
    };
    $.ajax({
        url: '/Home/IndexPostSearchResults',
        contentType: "application/json",
        type: "POST",
        data: JSON.stringify(data),
        dataType: 'html'
    })
        .done(function (result) {
            $('#post-result-id').html(result);
        });
}