﻿using AutoMapper;
using PetForum.Service.DTOs;
using PetForum.Service.DTOs.Comment;
using PetForum.Service.DTOs.User;
using PetForum.Web.Mvc.Models;

namespace PetForum.Mvc.ModelMapper
{
    public class MvcAutoMapping : Profile
    {
        public MvcAutoMapping()
        { 
            CreateMap<GetUserDto, UserViewModel>().ReverseMap();

            CreateMap<CreateUserDto, CreateUserViewModel>().ReverseMap();

            CreateMap<UpdateUserDto, UserViewModel>().ReverseMap();

            CreateMap<CreateCommentDto, CommentViewModel>().ReverseMap();

            CreateMap<GetCommentDto, CommentViewModel>().ReverseMap();

            CreateMap<UpdateCommentDto, CommentViewModel>().ReverseMap();

            CreateMap<CommentLikeDto, CommentLikeViewModel>().ReverseMap();

            CreateMap<PostLikeDto, PostLikeViewModel>().ReverseMap();

            CreateMap<CreatePostDto, PostViewModel>().ReverseMap();

            CreateMap<CreatePostDto, CreatePostViewModel>()
                .ForMember(destination => destination.CategoryName, opt => opt.Ignore())
                .ReverseMap();

            CreateMap<GetPostDto, PostViewModel>().ReverseMap();

            CreateMap<GetPostDto, IndexPostViewModel>()
                .ForMember(destination => destination.CommentsCount, opt => opt.Ignore())
                .ForMember(destination => destination.Likes, opt => opt.Ignore())
                .ReverseMap();

            CreateMap<UpdatePostDto, PostViewModel>().ReverseMap();

            CreateMap<CategoryDto, CategoryViewModel>().ReverseMap();
        }
    }
}