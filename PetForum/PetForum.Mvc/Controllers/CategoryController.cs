﻿using Microsoft.AspNetCore.Mvc;
using PetForum.Service.Services.Contracts;
using PetForum.Web.Mvc.Models;

namespace PetForum.Web.Mvc.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryService categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            this.categoryService = categoryService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]  
        public IActionResult Create()
        {
            var newCategory = new CategoryViewModel();

            return this.View(newCategory);
        }
    }
}