﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PetForum.Data.Database;
using PetForum.Service.DTOs;
using PetForum.Service.DTOs.User;
using PetForum.Service.Services;
using PetForum.Service.Services.Contracts;
using PetForum.Web.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PetForum.Web.Mvc.Controllers
{
    [Authorize]
    public class PostController : Controller
    {
        private readonly IPostService postService;
        private readonly IUserService userService;
        private readonly ICommentService commentService;
        private readonly IMapper mapper;
        private readonly PetForumContext context;

        public PostController(IUserService userService, IPostService postService, ICommentService commentService, IMapper mapper, PetForumContext context)
        {
            this.userService = userService;
            this.postService = postService;
            this.commentService = commentService;
            this.mapper = mapper;
            this.context = context;
        }

        public async Task<IActionResult> Details(int id)
        {
            var post = await postService.GetPostByIdAsync(id);
            var view = this.mapper.Map<PostViewModel>(post);
            return View("Details", view);
        }

        [HttpGet]
        public ActionResult Search()
        {
            return View(new SearchViewModel());
        }

        [HttpPost]
        public async Task<IActionResult> Search(SearchViewModel model)
        {
            try
            {
                if (model.SelectedCriteria.ToLower() == "users")
                {
                    List<GetUserDto> users = new List<GetUserDto>();

                    if (model.Keyword == null)
                    {
                        var allUsers = await this.userService.GetAllUsersAsync();
                        users = allUsers.OrderBy(u => u.DisplayName).ToList();
                    }
                    else
                    {
                        var searchedUsers = await this.userService.GetUserByUsernameAsync(model.Keyword);
                    }

                    var searchModel = new SearchViewModel
                    {
                        Users = users
                                   .Select(this.mapper.Map<UserSearchViewModel>)
                                   .ToList()
                    };

                    return View(searchModel);
                }
                else if (model.SelectedCriteria.ToLower() == "posts")
                {
                    var allPosts = await this.postService.GetPostByContentAsync(model.Keyword);

                    var searchModel = new SearchViewModel
                    {
                        Posts = allPosts
                              .Select(this.mapper.Map<PostViewModel>)
                              .ToList()
                    };

                    return View(searchModel);
                }
            }
            catch 
            {
                return RedirectToAction("Search");
            }

            return Ok();
        }

        [HttpGet]
        public IActionResult Create()
        {
            var viewModel = new CreatePostViewModel();
            return View("Create",viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreatePostViewModel postViewModel)
        {
            var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
            postViewModel.UserId = int.Parse(userId); 

            try
            {
                var view = this.mapper.Map<CreatePostDto>(postViewModel, opt => 
                opt.AfterMap((src, dest) => dest.CategoryType = postViewModel.CategoryName)); 
                await this.postService.CreatePostAsync(view);

                return RedirectToAction("Index", "Home");
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> PostsCount()
        {
            int count = await this.postService.GetPostsCount();
            var input = new PostViewModel();
            input.PostsCount = count;

            return this.View(input);
        }
    }   
}   