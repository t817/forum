﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PetForum.Service.Services.Contracts;
using System.Threading.Tasks;

namespace PetForum.Web.Mvc.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService userService;
        private readonly IMapper mapper;

        public UserController(IUserService userService, IMapper mapper)
        {
            this.userService = userService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index(int userId)
        {
            var user = await userService.GetUserByIdAsync(userId);
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> UsersCount()
        {
            int count = await this.userService.GetUsersCount();

            return this.View(count);
        }
    }
}