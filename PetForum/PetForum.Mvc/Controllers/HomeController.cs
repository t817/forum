﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PetForum.Mvc.Models;
using PetForum.Service.Services;
using PetForum.Service.Services.Contracts;
using PetForum.Web.Mvc.Models;
using PetForum.Web.Mvc.Models.JsonModels;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace PetForum.Mvc.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserService userService;
        private readonly ICommentService commentService;
        private readonly IPostService postService;
        private readonly IMapper mapper;

        public HomeController(IPostService postService, ICommentService commentService, IUserService userService, IMapper mapper)
        {
            this.postService = postService;
            this.commentService = commentService;
            this.userService = userService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var getPostCount = await this.postService.GetAllAsync();

            return View(getPostCount);
        }

        [HttpPost]
        public async Task<IActionResult> IndexPostSearchResults([FromBody] IndexSearchModel model)
        {
            var results = await postService.GetPostByTitleAsync(model.Keyword);
            var resultsViewModel = results.Select(post => mapper.Map<IndexPostViewModel>
              (post, opt => opt.AfterMap((src, dest) => dest.Likes = post.LikesCount)));

            var viewModel = new IndexListPostViewModel();

            viewModel.Posts.AddRange(resultsViewModel);

            return PartialView("_IndexPostsView", viewModel);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult HttpError()
        {
            int statusCode = this.HttpContext.Response.StatusCode;
            if (statusCode == 404)
            {
                var errorViewModel = new HttpErrorViewModel();
                errorViewModel.StatusCode = statusCode;
                errorViewModel.Message = "There is no page in our system";
                return this.View(errorViewModel);
            }

            return this.View(
                "Error", new ErrorViewModel { RequestId = Activity.Current?.Id ?? this.HttpContext.TraceIdentifier });
        }
    }
}