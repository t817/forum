﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PetForum.Service.DTOs.User;
using PetForum.Service.Services.Contracts;
using PetForum.Web.Mvc.Helpers;
using PetForum.Web.Mvc.Models;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PetForum.Web.Mvc.Controllers
{
    public class AuthController : Controller
    {
        private readonly IUserService userService;
        private readonly IMapper mapper;
        private readonly IAuthHelper authHelper;

        public AuthController(IUserService userService, IMapper mapper, IAuthHelper authHelper)
        {
            this.userService = userService;
            this.mapper = mapper;
            this.authHelper = authHelper;
        }

        [HttpGet]
        public IActionResult Login()
        {
            var model = new LogInViewModel();
            return View("Login", model);
        }

        [HttpPost]
        public async Task<IActionResult> Login(LogInViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View("Login", model);
            }

            var user = await this.authHelper.TryGetUser(model.Email, model.Password);

            if (user != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim(ClaimTypes.Name, $"{user.FirstName} {user.LastName}"),
                    new Claim(ClaimTypes.Role, user.Role)
                };

                var claimsIdentity = new ClaimsIdentity(claims, "LogIn");

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                                                            new ClaimsPrincipal(claimsIdentity));

                return this.RedirectToAction("Index", "Home");
            }

            return this.View(model);
        }

        [HttpGet]
        public IActionResult Register()
        {
            var vm = new CreateUserViewModel();
            return View("Register", vm);
        }

        [HttpPost]
        public async Task<IActionResult> Register(CreateUserViewModel viewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return View("Register", viewModel);
            }

            var input = this.mapper.Map<CreateUserDto>(viewModel);
            await this.userService.CreateUserAsync(input);

            return RedirectToAction("Index", "Home");

        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await this.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);  //?

            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> CheckIfUsernameAvailable(string username)
        {
            var user = await this.userService.GetUserByUsernameAsync(username);
            if (user == null)
            {
                return Json("available");
            }
            else
            {
                return Json("unavailable");
            }
        }
    }
}