﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PetForum.Service.DTOs;
using PetForum.Service.DTOs.Comment;
using PetForum.Service.Services;
using PetForum.Service.Services.Contracts;
using PetForum.Web.Mvc.Models;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PetForum.Web.Mvc.Controllers
{
    public class CommentController : Controller
    {
        private readonly ICommentService commentService;
        private readonly IPostService postService;
        private readonly IMapper mapper;

        public CommentController(ICommentService commentService, IPostService postService, IMapper mapper)
        {
            this.commentService = commentService;
            this.postService = postService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var comments = await this.commentService.GetAllCommentsAsync();

            var viewModel = this.mapper.Map<CommentViewModel>(comments);

            return View(viewModel);
        }

        public async Task<IActionResult> Details(int id)
        {
            var comment = await this.commentService.GetCommentByIdAsync(id);

            if (comment == null)
            {
                return NotFound();
            }

            var viewModel = this.mapper.Map<CommentViewModel>(comment);

            return View(viewModel);
        }

        public ActionResult Create()
        {
            var comment = new CreateCommentViewModel();

            return View(comment);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PostViewModel viewModel)
        {
            var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);

            await this.commentService.CreateCommentAsync(viewModel.Id, int.Parse(userId), viewModel.NewComment);

            return this.RedirectToAction("Details", "Post", new { id = viewModel.Id });
        }

        public async Task<IActionResult> Edit(int id, string content)
        {
            var view = new UpdateCommentViewModel();
            view.Id = id;
            view.Content = content;

            return this.View(view);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, UpdateCommentViewModel viewModel)
        {
            try
            {
                var map = this.mapper.Map<UpdateCommentDto>(viewModel);

                var model = await this.commentService.UpdateCommentAsync(id, map);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View(viewModel);
            }
        }
        public IActionResult Delete(int id, CommentViewModel viewModel)
        {
            try
            {
                var parcel = this.commentService.DeleteCommentAsync(id); 

                var view = this.mapper.Map<CreateCommentDto>(parcel);

                return this.View(view);
            }
            catch (ArgumentException )
            {
                return this.CommentNotFound(id);
            }
        }

        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int id)
        {
            try
            {
                this.commentService.DeleteCommentAsync(id);
            }
            catch (ArgumentException)
            {
                return this.CommentNotFound(id);
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        private IActionResult CommentNotFound(int id)
        {
            this.Response.StatusCode = StatusCodes.Status404NotFound;
            this.ViewData["error"] = $"Comment with id: {id} does not exist.";
            return this.View("Error");
        }
    }
}