﻿using PetForum.Service.DTOs.User;
using System.Threading.Tasks;

namespace PetForum.Web.Mvc.Helpers
{
    public interface IAuthHelper
    {
        Task<GetUserDto> TryGetUser(string email, string password);
    }
}