﻿using PetForum.Service.DTOs.User;
using PetForum.Service.Services.Contracts;
using System.Threading.Tasks;

namespace PetForum.Web.Mvc.Helpers
{
    public class AuthHelper : IAuthHelper
    {
        private readonly IUserService service;

        public AuthHelper(IUserService service)
        {
            this.service = service;
        }

        public async Task<GetUserDto> TryGetUser(string email, string password)
        {
            var user = await this.service.GetUserByEmailAsync(email);

            return user;
        }
    }
}