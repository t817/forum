﻿using PetForum.Data;
using PetForum.Data.Models;
using System.Collections.Generic;

namespace PetForum.Web.Mvc.Models
{
    public class UserViewModel
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        public string DisplayName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Role { get; set; }

        public bool IsBlocked { get; set; }

        public UserPhoto UserPhoto { get; set; }

        public ICollection<Post> Posts { get; set; } = new List<Post>();

        public ICollection<Comment> Comments { get; set; } = new List<Comment>();

        public ICollection<PostLike> PostLikes { get; set; } = new List<PostLike>();

        public ICollection<CommentLike> CommentLikes { get; set; } = new List<CommentLike>();

        public ICollection<Notification> Notifications { get; set; } = new List<Notification>();
    }
}