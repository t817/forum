﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;

namespace PetForum.Web.Mvc.Models
{
    public class CreatePostViewModel
    {
        public CreatePostViewModel()
        {
            this.Categories = new List<SelectListItem>
            {
                new SelectListItem("Dogs", "Dogs"),
                new SelectListItem("Cats", "Cats"),
                new SelectListItem("Other pets", "Other pets")
            };
        }

        public int UserId { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public string CategoryName { get; set; }

        public List<SelectListItem> Categories { get; set; }

        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;
    }
}