﻿using System.Collections.Generic;

namespace PetForum.Web.Mvc.Models
{
    public class IndexViewModel
    {
        public string Name { get; set; }

        public List<PostViewModel> Posts { get; set; }

        public List<CommentViewModel> Comments { get; set; }
    }
}