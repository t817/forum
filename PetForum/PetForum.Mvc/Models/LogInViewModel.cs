﻿using System.ComponentModel.DataAnnotations;

namespace PetForum.Web.Mvc.Models
{
    public class LogInViewModel
    {
        [Required(ErrorMessage = "Invalid value for {0}!")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Invalid value for {0}!")]
        public string Password { get; set; }
    }
}