﻿using Newtonsoft.Json;

namespace PetForum.Web.Mvc.Models.JsonModels
{
    public class IndexSearchModel
    {
        [JsonProperty("keyword")]
        public string Keyword { get; set; }
    }
}