﻿namespace PetForum.Web.Mvc.Models
{
    public class PostLikeViewModel
    {
        public int PostId { get; set; }

        public int UserId { get; set; }
    }
}