﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PetForum.Web.Mvc.Models
{
    public class CommentViewModel
    {
        public int Id { get; set; }

        [Required]
        public string Content { get; set; }

        public int UserId { get; set; }

        public int PostId { get; set; }

        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;

        public string NewComment { get; set; }
       
        public bool IsLiked { get; set; }

        public ICollection<CommentLikeViewModel> Likes { get; set; } = new List<CommentLikeViewModel>();
    }
}