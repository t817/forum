﻿namespace PetForum.Web.Mvc.Models
{
    public class CommentLikeViewModel
    {
        public int UserId { get; set; }

        public int CommentId { get; set; }
    }
}