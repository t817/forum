﻿namespace PetForum.Web.Mvc.Models
{
    public class UserSearchViewModel
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public string DisplayName { get; set; }

        public string Email { get; set; }
    }
}