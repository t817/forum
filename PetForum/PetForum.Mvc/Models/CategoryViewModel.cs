﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PetForum.Web.Mvc.Models
{
    public class CategoryViewModel
    {
        [Required]
        public string Type { get; set; }

        public ICollection<PostViewModel> Posts { get; set; } = new List<PostViewModel>();
    }
}