﻿namespace PetForum.Web.Mvc.Models
{
    public class CreateCommentViewModel
    {
        public string Content { get; set; }

        public int PostId { get; set; }

        public int UserId { get; set; }
    }
}