﻿namespace PetForum.Web.Mvc.Models
{
    public class HttpErrorViewModel
    {
        public int StatusCode { get; set; }

        public string Message { get; set; }
    }
}