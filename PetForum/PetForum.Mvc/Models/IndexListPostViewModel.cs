﻿using System.Collections.Generic;

namespace PetForum.Web.Mvc.Models
{
    public class IndexListPostViewModel
    {
        public IndexListPostViewModel()
        {
            Posts = new List<IndexPostViewModel>();
            PostsCount = Posts.Count;
        }

        public string Keyword { get; set; }
        public int PostsCount { get; set; }
        public List<IndexPostViewModel> Posts { get; set; }
    }
}