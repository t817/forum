﻿using System.Collections.Generic;

namespace PetForum.Web.Mvc.Models
{
    public class SearchViewModel
    {
        public string SelectedCriteria { get; set; }

        public string Keyword { get; set; }

        public string SelectedOrderBy { get; set; }

        public ICollection<UserSearchViewModel> Users { get; set; } = new List<UserSearchViewModel>();

        public ICollection<PostViewModel> Posts { get; set; } = new List<PostViewModel>();
    }
}