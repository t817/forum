﻿using PetForum.Data.Enums;
using System.ComponentModel.DataAnnotations;

namespace PetForum.Web.Mvc.Models
{
    public class CreateUserViewModel
    {
        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string Username { get; set; }

        [Required, RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)\S{8,30}$", ErrorMessage = "Invalid Password")]
        public string Password { get; set; }
   
        [Required, RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)\S{8,30}$", ErrorMessage = "Invalid Password")]
        public string ConfirmPassword { get; set; }

        public string Email { get; set; } 

        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string DisplayName { get; set; }

        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string FirstName { get; set; }

        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string LastName { get; set; }

        [Required]
        public string Role { get; set; } = UserRole.Member.ToString();
    }
}