﻿namespace PetForum.Web.Mvc.Models
{
    public class UpdateCommentViewModel
    {
        public int Id { get; set; }

        public string Content { get; set; } 
    }
}