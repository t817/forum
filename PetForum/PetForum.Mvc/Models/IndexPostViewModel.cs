﻿using System;

namespace PetForum.Web.Mvc.Models
{
    public class IndexPostViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public string UserName { get; set; }

        public int UserId { get; set; }

        public int CategoryId { get; set; }

        public string Category { get; set; }

        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;

        public int Likes { get; set; }

        public int CommentsCount { get; set; }

        public DateTime ModifiedOn { get; set; }
    }
}