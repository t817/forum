﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PetForum.Web.Mvc.Models
{
    public class PostViewModel
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Content { get; set; }

        public int UserId { get; set; }

        public string DisplayName { get; set; }

        public int CategoryId { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool IsLiked { get; set; }

        public int PostsCount { get; set; }

        public string NewComment { get; set; }

        public ICollection<PostLikeViewModel> Likes { get; set; } = new List<PostLikeViewModel>();

        public ICollection<CommentViewModel> Comments { get; set; } = new List<CommentViewModel>();
    }
}