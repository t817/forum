﻿using Newtonsoft.Json;
using PetForum.Data.Models;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PetForum.Data.Database
{
    public static class ModelBuilderExtension
    {
        public static async Task EnsureSeeded(this PetForumContext context)
        {
            if (context.Users.Any())
            {
                return;
            }

            var usersAsJson = File.ReadAllText(@"..\PetForum.Data\Seed\Users.json");
            var categoriesAsJson = File.ReadAllText(@"..\PetForum.Data\Seed\Categories.json");
            var postsAsJson = File.ReadAllText(@"..\PetForum.Data\Seed\Posts.json");

            var users = JsonConvert.DeserializeObject<User[]>(usersAsJson);
            var categories = JsonConvert.DeserializeObject<Category[]>(categoriesAsJson);
            var posts = JsonConvert.DeserializeObject<Post[]>(postsAsJson);

            context.Users.AddRange(users);

            context.Categories.AddRange(categories);

            await context.SaveChangesAsync();

            context.Posts.AddRange(posts);

            await context.SaveChangesAsync();
        }
    }
}