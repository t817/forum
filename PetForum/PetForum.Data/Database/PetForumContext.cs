﻿using Microsoft.EntityFrameworkCore;
using PetForum.Data.Models;
using System.Threading;
using System.Threading.Tasks;

namespace PetForum.Data.Database
{
    public class PetForumContext : DbContext
    {
        public PetForumContext(DbContextOptions<PetForumContext> options)
            : base(options)
        {

        }

        public DbSet<User> Users { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Post> Posts { get; set; }

        public DbSet<PostLike> PostLikes { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<CommentLike> CommentLikes { get; set; }

        public DbSet<UserPhoto> UserPhotos { get; set; }

        public DbSet<Notification> Notifications { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasIndex(u => u.Email)
                .IsUnique(true);

            modelBuilder.Entity<User>()
                .HasIndex(u => u.Username)
                .IsUnique(true);

            modelBuilder.Entity<Notification>().HasOne(n => n.User).WithMany(n => n.Notifications)
                .HasForeignKey(n => n.UserId).OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<UserPhoto>().HasOne(u => u.User).WithOne(u => u.UserPhoto)
                .HasForeignKey<UserPhoto>(u => u.UserId).OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Post>().HasOne(p => p.Category).WithMany(p => p.Posts)
                .HasForeignKey(p => p.CategoryId).OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Post>().HasMany(p => p.Comments).WithOne(p => p.Post)
                .HasForeignKey(p => p.PostId).OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Post>().HasMany(p => p.Likes).WithOne(p => p.Post)
                .HasForeignKey(p => p.PostId).OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Post>().HasOne(p => p.User).WithMany(p => p.Posts)
                .HasForeignKey(p => p.UserId).OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<PostLike>().HasOne(p => p.User).WithMany(p => p.PostLikes)
                .HasForeignKey(p => p.UserId).OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<PostLike>().HasOne(p => p.Post).WithMany(p => p.Likes)
                .HasForeignKey(p => p.PostId).OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Comment>().HasOne(c => c.User).WithMany(c => c.Comments)
                .HasForeignKey(p => p.UserId).OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Comment>().HasOne(c => c.Post).WithMany(c => c.Comments)
                .HasForeignKey(p => p.PostId).OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<CommentLike>().HasOne(c => c.User).WithMany(c => c.CommentLikes)
                .HasForeignKey(p => p.UserId).OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<CommentLike>().HasOne(c => c.Comment).WithMany(c => c.Likes)
                .HasForeignKey(p => p.CommentId).OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<User>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<Category>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<Post>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<Comment>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<PostLike>().HasQueryFilter(x => !x.IsDeleted);
            modelBuilder.Entity<CommentLike>().HasQueryFilter(x => !x.IsDeleted);
        }

        public override Task<int> SaveChangesAsync
         (bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            UpdateSoftDeleteStatuses();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void UpdateSoftDeleteStatuses()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.CurrentValues["IsDeleted"] = false;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Modified;
                        entry.CurrentValues["IsDeleted"] = true;
                        break;
                }
            }
        }
    }
}