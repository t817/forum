﻿namespace PetForum.Data.Enums
{
    public enum UserRole
    {
        Admin,
        Member
    }
}