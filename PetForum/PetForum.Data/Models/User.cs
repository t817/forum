﻿using PetForum.Data.Models.Abstracts;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PetForum.Data.Models
{
    public class User : Entity
    {
        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string Username { get; set; }

        [Required, RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)\S{8,30}$", ErrorMessage = "Invalid Password")]
        public string Password { get; set; }

        [Required, EmailAddress(ErrorMessage = "Invalid value for {0}!")]
        public string Email { get; set; }

        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string DisplayName { get; set; }

        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string FirstName { get; set; }

        [Required, StringLength(20, MinimumLength = 2, ErrorMessage = "{0} value must be between {2} and {1} symbols!")]
        public string LastName { get; set; }

        [Required] 
        public string Role { get; set; }

        public bool IsBlocked { get; set; }

        public UserPhoto UserPhoto { get; set; }

        public ICollection<Post> Posts { get; set; } = new List<Post>();

        public ICollection<Comment> Comments { get; set; } = new List<Comment>();

        public ICollection<PostLike> PostLikes { get; set; } = new List<PostLike>();

        public ICollection<CommentLike> CommentLikes { get; set; } = new List<CommentLike>();

        public ICollection<Notification> Notifications { get; set; } = new List<Notification>();
    }
}