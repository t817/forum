﻿using PetForum.Data.Models.Abstracts;

namespace PetForum.Data.Models
{
    public class PostLike : Entity
    {
        public int PostId { get; set; }

        public Post Post { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }
    }
}