﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PetForum.Data.Models.Abstracts
{
    public abstract class Entity
    {
        [Key]
        public int Id { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;

        public DateTime? ModifiedOn { get; set; }
    }
}