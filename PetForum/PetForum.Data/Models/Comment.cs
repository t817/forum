﻿using PetForum.Data.Models.Abstracts;
using System.Collections.Generic;

namespace PetForum.Data.Models
{
    public class Comment : Entity
    {
        public string Content { get; set; }

        public int PostId { get; set; }

        public Post Post { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }

        public ICollection<CommentLike> Likes { get; set; } = new List<CommentLike>();
    }
}