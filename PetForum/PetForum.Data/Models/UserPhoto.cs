﻿using System.ComponentModel.DataAnnotations;

namespace PetForum.Data.Models
{
    public class UserPhoto
    {
        [Key]
        public int Id { get; set; }

        public string PhotoUrl { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }
    }
}