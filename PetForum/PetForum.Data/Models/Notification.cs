﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PetForum.Data.Models
{
    public class Notification
    {
        [Key]
        public int Id { get; set; }

        public bool IsDeleted { get; set; }

        public string Text { get; set; }

        public DateTime CreatedOn { get; set; }

        public Byte Seen { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }
    }
}