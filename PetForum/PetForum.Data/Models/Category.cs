﻿using PetForum.Data.Models.Abstracts;
using System.Collections.Generic;

namespace PetForum.Data.Models
{
    public class Category : Entity
    {
        public string Type { get; set; }

        public ICollection<Post> Posts { get; set; } = new List<Post>();
    }
}