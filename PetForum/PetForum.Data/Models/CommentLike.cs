﻿using PetForum.Data.Models.Abstracts;

namespace PetForum.Data.Models
{
    public class CommentLike : Entity
    {
        public int UserId { get; set; }

        public User User { get; set; }

        public int CommentId { get; set; }

        public Comment Comment { get; set; }
    }
}