﻿using PetForum.Data.Models;
using PetForum.Data.Models.Abstracts;
using System.Collections.Generic;

namespace PetForum.Data
{
    public class Post : Entity
    {
        public string Title { get; set; }

        public string Content { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }

        public int CategoryId { get; set; }

        public Category Category { get; set; }

        public ICollection<Comment> Comments { get; set; } = new List<Comment>();

        public ICollection<PostLike> Likes { get; set; } = new List<PostLike>();
    }
}