﻿namespace PetForum.Service.Enums
{
    public enum PostsSort
    {
        CreatedOn,
        ModifiedOn,
        Category
    }
}