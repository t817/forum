﻿namespace PetForum.Service.Enums
{
    public enum CategoryType
    {
        Dogs,
        Cats,
        OtherPets
    }
}