﻿namespace PetForum.Service.DTOs
{
    public class PostLikeDto
    {
        public int PostId { get; set; }

        public int UserId { get; set; }
    }
}