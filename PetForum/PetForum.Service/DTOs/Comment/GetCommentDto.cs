﻿using System;
using System.Collections.Generic;

namespace PetForum.Service.DTOs
{
    public class GetCommentDto
    {
        public string Content { get; set; }

        public int PostId { get; set; }

        public int UserId { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public int LikesCount { get; set; }

        public ICollection<CommentLikeDto> Likes { get; set; } = new List<CommentLikeDto>();
    }
}