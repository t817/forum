﻿using System;

namespace PetForum.Service.DTOs
{
    public class CreateCommentDto
    {
        public string Content { get; set; }

        public int PostId { get; set; }

        public int UserId { get; set; }

        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;
    }
}