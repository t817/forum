﻿namespace PetForum.Service.DTOs
{
    public class CommentLikeDto
    {
        public int UserId { get; set; }

        public int CommentId { get; set; }
    }
}