﻿using System;

namespace PetForum.Service.DTOs.Comment
{
    public class UpdateCommentDto
    {
        public int Id { get; set; }

        public string Content { get; set; }

        public DateTime ModifiedOn { get; set; }
    }
}