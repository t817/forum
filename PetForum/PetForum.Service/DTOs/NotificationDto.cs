﻿using System;

namespace PetForum.Service.DTOs
{
    public class NotificationDto
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public DateTime CreatedOn { get; set; } =  DateTime.UtcNow;

        public Byte Seen { get; set; }

        public int UserId { get; set; }
    }
}