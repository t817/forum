﻿using System;

namespace PetForum.Service.DTOs.User
{
    public class UpdateUserDto
    {
        public string Username { get; set; }

        public string Password { get; set; } 

        public string Email { get; set; }

        public string DisplayName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime ModifiedOn { get; set; } = DateTime.Now;
    }
}