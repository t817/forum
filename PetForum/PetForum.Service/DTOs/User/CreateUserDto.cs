﻿using PetForum.Data.Enums;
using System;

namespace PetForum.Service.DTOs.User
{
    public class CreateUserDto
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        public string DisplayName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Role { get; set; } = UserRole.Member.ToString();

        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;
    }
}