﻿namespace PetForum.Service.DTOs.User
{
    public class SearchUserByDto
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
    }
}