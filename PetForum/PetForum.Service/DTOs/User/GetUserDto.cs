﻿using Newtonsoft.Json;
using PetForum.Data.Models;
using System;
using System.Collections.Generic;

namespace PetForum.Service.DTOs.User
{
    public class GetUserDto
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public string DisplayName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Role { get; set; }

        public bool IsBlocked { get; set; }

        [JsonIgnore]
        public string UserPhotoUrl { get; set; }

        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;

        [JsonIgnore]
        public DateTime? ModifiedOn { get; set; }

        public ICollection<GetPostDto> Posts { get; set; }

        public ICollection<GetCommentDto> Comments { get; set; }

        public ICollection<PostLikeDto> PostLikes { get; set; }

        public ICollection<CommentLikeDto> CommentLikes { get; set; }

        public ICollection<Notification> Notifications { get; set; }
    }
}