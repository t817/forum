﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PetForum.Service.DTOs
{
    public class GetPostDto
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public int UserId { get; set; }

        public string Username { get; set; }

        public int CategoryId { get; set; }

        public string Category { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime CreatedOn { get; set; }

        [JsonIgnore]
        public DateTime? ModifiedOn { get; set; }

        public int LikesCount { get; set; }

        public ICollection<GetCommentDto> Comments { get; set; } = new List<GetCommentDto>();
    }
}