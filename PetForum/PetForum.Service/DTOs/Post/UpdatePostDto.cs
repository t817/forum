﻿using System;

namespace PetForum.Service.DTOs
{
    public class UpdatePostDto
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}