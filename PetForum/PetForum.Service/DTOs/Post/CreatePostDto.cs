﻿using System;

namespace PetForum.Service.DTOs
{
    public class CreatePostDto
    {
        public string Title { get; set; }

        public string Content { get; set; }

        public int UserId { get; set; }

        public string CategoryType { get; set; }

        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;
    }
}