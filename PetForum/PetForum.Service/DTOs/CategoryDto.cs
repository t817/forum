﻿using System;
using System.Collections.Generic;

namespace PetForum.Service.DTOs
{
    public class CategoryDto
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;

        public DateTime? ModifiedOn { get; set; }

        public ICollection<CreatePostDto> Posts { get; set; } = new List<CreatePostDto>();
    }
}