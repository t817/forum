﻿using AutoMapper;
using PetForum.Data;
using PetForum.Data.Models;
using PetForum.Service.DTOs;
using PetForum.Service.DTOs.Comment;
using PetForum.Service.DTOs.User;

namespace PetForum.Service.ModelMapper
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<User, GetUserDto>().ReverseMap();

            CreateMap<User, CreateUserDto>().ReverseMap();

            CreateMap<User, UpdateUserDto>().ReverseMap();

            CreateMap<Comment, CreateCommentDto>().ReverseMap();

            CreateMap<Comment, GetCommentDto>()
                .ForMember(destination => destination.LikesCount, opt => opt.Ignore()).ReverseMap();

            CreateMap<Comment, UpdateCommentDto>().ReverseMap();

            CreateMap<CommentLike, CommentLikeDto>().ReverseMap();

            CreateMap<PostLike, PostLikeDto>().ReverseMap();

            CreateMap<Post, CreatePostDto>().ReverseMap();

            CreateMap<Post, GetPostDto>()
                .ForMember(destination => destination.LikesCount, opt => opt.Ignore())
            .ForMember(dest => dest.CategoryId, opt => opt.MapFrom(src => src.CategoryId))
            .ForMember(dest => dest.Category, opt => opt.MapFrom(src => src.Category.Type))
            .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
            .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.User.DisplayName)).ReverseMap();

            CreateMap<Post, UpdatePostDto>().ReverseMap();

            CreateMap<Category, CategoryDto>().ReverseMap();

            CreateMap<Notification, NotificationDto>().ReverseMap();
        }
    }
}