﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PetForum.Data.Database;
using PetForum.Data.Models;
using PetForum.Service.DTOs;
using PetForum.Service.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace PetForum.Service.Services
{
    public class NotificationService : INotificationService
    {
        private readonly PetForumContext context;
        private readonly IMapper mapper;

        public NotificationService(PetForumContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<NotificationDto> CreateNotificationAsync(NotificationDto notificationDto)
        {
            var user = await this.context.Users.FirstOrDefaultAsync(u => u.Id == notificationDto.UserId) ?? 
                                                    throw new ApplicationException(Exceptions.EntityNotFound);

            var notification = this.mapper.Map<Notification>(notificationDto);

            this.context.Notifications.Add(notification);

            await this.context.SaveChangesAsync();

            return notificationDto;
        }
    }
}