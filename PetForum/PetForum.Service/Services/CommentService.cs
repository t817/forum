﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PetForum.Data.Database;
using PetForum.Data.Models;
using PetForum.Service.DTOs;
using PetForum.Service.DTOs.Comment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetForum.Service.Services
{
    public class CommentService : ICommentService
    {
        private readonly PetForumContext context;
        private readonly IMapper mapper;

        public CommentService(PetForumContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<CreateCommentDto> CreateCommentAsync(CreateCommentDto commentDto)
        {
            var user = await this.context.Users.FirstOrDefaultAsync(u => u.Id == commentDto.UserId) ??
                             throw new ApplicationException(Exceptions.EntityNotFound);

            var post = await this.context.Posts.FirstOrDefaultAsync(p => p.Id == commentDto.PostId) ?? 
                             throw new ApplicationException(Exceptions.EntityNotFound);

            var comment = this.mapper.Map<Comment>(commentDto);

            this.context.Comments.Add(comment);
            user.Comments.Add(comment);
            post.Comments.Add(comment);

            await this.context.SaveChangesAsync();

            return commentDto;
        }

        public async Task CreateCommentAsync(int postId, int userId, string newCommentContent)
        {
            var newComment = new Comment();
            newComment.PostId = postId;
            newComment.UserId = userId;
            newComment.Content = newCommentContent;

            this.context.Comments.Add(newComment);

            await context.SaveChangesAsync();
        }

        public async Task DeleteCommentAsync(int id)
        {
            var comment = await this.context.Comments.FirstOrDefaultAsync(c => c.Id == id) ??
                                throw new ApplicationException(Exceptions.EntityNotFound);

            this.context.Comments.Remove(comment);

            await this.context.SaveChangesAsync();
        }

        public async Task<UpdateCommentDto> UpdateCommentAsync(int id, UpdateCommentDto commentDto)
        {
            var commentToUpdate = await this.context.Comments.FirstOrDefaultAsync(c => c.Id == id) ??
                                        throw new ApplicationException(Exceptions.EntityNotFound);

            if (commentDto.Content != null)
            {
                commentToUpdate.Content = commentDto.Content;
                commentToUpdate.ModifiedOn = DateTime.UtcNow;
            }

            var comment = this.mapper.Map<UpdateCommentDto>(commentToUpdate);

            await this.context.SaveChangesAsync();

            return comment;
        }

        public async Task<IEnumerable<GetCommentDto>> GetAllCommentsAsync()
        {
            var comments = await this.context.Comments.ToListAsync();


            var commentDtos = comments.Select(comment => mapper.Map<GetCommentDto>
                        (comment, opt => opt.AfterMap((src, dest) => dest.LikesCount = GetLikesCount(comment.Id))));

            return !commentDtos.Any() ? throw new ApplicationException(Exceptions.EntitiesNotFound) : commentDtos;
        }

        public async Task<GetCommentDto> GetCommentByIdAsync(int id)
        {
            var comment = await this.context.Comments.FirstOrDefaultAsync(c => c.Id == id) ??
                                throw new ApplicationException(Exceptions.EntityNotFound);

            return this.mapper.Map<GetCommentDto>(comment);
        }

        public async Task AddLikeToComment(int Id, CommentLikeDto commentLike)
        {
            var comment = await this.GetCommentByIdAsync(Id);

            if (comment == null)
            {
                throw new ApplicationException(Exceptions.EntityNotFound);
            }

            comment.Likes.Add(commentLike);

            this.context.SaveChanges();
        }

        public int GetLikesCount(int commentId)
        {
            var likesCount = this.context.Comments
                .Where(c => c.Id == commentId)
                .Include(c => c.Likes)
                .Select(c => c.Likes)
                .Count();

            return likesCount;
        }
    }
}