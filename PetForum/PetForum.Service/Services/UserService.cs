﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PetForum.Data.Database;
using PetForum.Data.Models;
using PetForum.Service.DTOs.User;
using PetForum.Service.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetForum.Service.Services
{
    public class UserService : IUserService
    {
        private readonly PetForumContext context;
        private readonly IMapper mapper;
        private const string emailPattern = "@petforum.com";

        public UserService(PetForumContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<GetUserDto> GetUserByIdAsync(int id)
        {
            var user = await this.context.Users
                .Include(u => u.Posts)
                .ThenInclude(p => p.Category)
                .Include(u => u.Posts)
                .Include(u => u.Comments)
                .FirstOrDefaultAsync(u => u.Id == id) ??
                                        throw new ApplicationException(Exceptions.EntityNotFound);

            return mapper.Map<GetUserDto>(user);
        }

        public async Task<GetUserDto> GetUserByUsernameAsync(string username)
        {
            var user = await this.context.Users.FirstOrDefaultAsync(u => u.Username == username) ??
                                        throw new ApplicationException(Exceptions.EntityNotFound);

            return mapper.Map<GetUserDto>(user);
        }

        public async Task<GetUserDto> GetUserByEmailAsync(string email)
        {
            var user = await this.context.Users.FirstOrDefaultAsync(u => u.Email == email) ??
                                        throw new ApplicationException(Exceptions.EntityNotFound);

            return mapper.Map<GetUserDto>(user);
        }

        public async Task<IEnumerable<GetUserDto>> GetUsersByDisplayNameAsync(string displayName)
        {
            var users = await this.context.Users.Where(u => u.DisplayName == displayName)
                                                .Select(u => mapper.Map<GetUserDto>(u))
                                                .ToListAsync();

            return !users.Any() ? throw new ApplicationException(Exceptions.EntitiesNotFound) : users;
        }

        public async Task<IEnumerable<GetUserDto>> GetAllUsersAsync()
        {
            var users = await this.context.Users.Select(u => mapper.Map<GetUserDto>(u)).ToListAsync();

            return !users.Any() ? throw new ApplicationException(Exceptions.EntitiesNotFound) : users;
        }

        public async Task<CreateUserDto> CreateUserAsync(CreateUserDto userDto)
        {
            var user = this.mapper.Map<User>(userDto);
            await this.context.Users.AddAsync(user);
            await this.context.SaveChangesAsync();

            return userDto;
        }

        public async Task<UpdateUserDto> UpdateUserAsync(int id, UpdateUserDto updateUserDto)
        {
            var userToUpdate = await this.context.Users.FirstOrDefaultAsync(u => u.Id == id) ??
                                         throw new ApplicationException(Exceptions.EntityNotFound);

            if (updateUserDto.Email != null && updateUserDto.Email.EndsWith(emailPattern))
            {
                throw new ApplicationException($"You can not use company email: {emailPattern} for registration!");
            }

            if (context.Users.Any(u => u.Id != userToUpdate.Id && u.Email == updateUserDto.Email))
            {
                throw new ApplicationException($"Email: {updateUserDto.Email} already exists in database!");
            }

            userToUpdate.Email = updateUserDto.Email;

            if (context.Users.Any(u => u.Id != userToUpdate.Id && u.Username == updateUserDto.Username))  
            {
                throw new ApplicationException($"Username: {updateUserDto.Username} already exists in database!");
            }

            userToUpdate.Username = updateUserDto.Username;

            if (!string.IsNullOrEmpty(updateUserDto.DisplayName))
            {
                userToUpdate.DisplayName = updateUserDto.DisplayName;
            }
            if (!string.IsNullOrEmpty(updateUserDto.FirstName))
            {
                userToUpdate.FirstName = updateUserDto.FirstName;
            }
            if (!string.IsNullOrEmpty(updateUserDto.LastName))
            {
                userToUpdate.LastName = updateUserDto.LastName;
            }

            await this.context.SaveChangesAsync();
            var userDto = mapper.Map<UpdateUserDto>(userToUpdate);

            return userDto;
        }

        public async Task DeleteUserAsync(int id)
        {
            var user = await this.context.Users.FirstOrDefaultAsync(u => u.Id == id) ??
                                        throw new ApplicationException(Exceptions.EntityNotFound);

            this.context.Users.Remove(user);

            await this.context.SaveChangesAsync();
        }

        public async Task<bool> BlockUserAsync(int id)
        {
            var user = await this.context.Users.FirstOrDefaultAsync(u => u.Id == id) ??
                                      throw new ApplicationException(Exceptions.EntityNotFound);

            if (user.IsBlocked)
            {
                return false;
            }

            user.IsBlocked = true;
            await this.context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UnblockUserAsync(int id)
        {
            var user = await this.context.Users.FirstOrDefaultAsync(u => u.Id == id) ??
                                      throw new ApplicationException(Exceptions.EntityNotFound);

            if (!user.IsBlocked)
            {
                return false;
            }

            user.IsBlocked = false;
            await this.context.SaveChangesAsync();

            return true;
        }

        public async Task<int> GetUsersCount()
        {
            return await this.context.Users.CountAsync();

        }
    }
}