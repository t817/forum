﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PetForum.Data;
using PetForum.Data.Database;
using PetForum.Data.Enums;
using PetForum.Data.Models;
using PetForum.Service.DTOs;
using PetForum.Service.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetForum.Service.Services
{
    public class PostService : IPostService
    {
        private readonly PetForumContext context;
        private readonly IMapper mapper;

        public PostService(PetForumContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<CreatePostDto> CreatePostAsync(CreatePostDto postDto)
        {
            var user = this.context.Users.FirstOrDefault(u => u.Id == postDto.UserId);

            if (user.IsBlocked)
            {
                throw new ApplicationException("You are blocked and can't create post!");
            }

            var post = this.mapper.Map<Post>(postDto);

            post.UserId = postDto.UserId;
            post.CategoryId = await this.context.Categories.Where(c => c.Type == postDto.CategoryType).Select(c => c.Id).FirstOrDefaultAsync();

            post.Title = postDto.Title;
            post.CreatedOn = postDto.CreatedOn = DateTime.UtcNow;

            await this.context.Posts.AddAsync(post);

            await this.context.SaveChangesAsync();

            return this.mapper.Map<CreatePostDto>(post);
        }

        public async Task<GetPostDto> GetPostByIdAsync(int id)
        {
            var post = await this.context.Posts
                 .Include(p => p.User)
                 .Include(p => p.Category)
                 .Include(p => p.User)
                 .Include(u => u.Comments)
                 .FirstOrDefaultAsync(p => p.Id == id)        
                ?? throw new ApplicationException(Exceptions.EntityNotFound);

            return this.mapper.Map<GetPostDto>(post);
        }

        public async Task<IEnumerable<GetPostDto>> GetUserPostsAsync(int userId)
        {
            var user = this.context.Users.FirstOrDefault(u => u.Id == userId) ?? throw new ApplicationException(Exceptions.EntityNotFound);

            var posts = await this.context.Posts.Where(p => p.UserId == userId)
                                                .Select(p => mapper.Map<GetPostDto>(p))
                                                .ToListAsync();

            return !posts.Any() ? throw new ApplicationException(Exceptions.EntitiesNotFound) : posts.OrderByDescending(p => p.CreatedOn);
        }

        public async Task<IEnumerable<GetPostDto>> GetPostByContentAsync(string keyword)
        {
            var result = await this.context.Posts.Where(p => p.Content.Contains(keyword))
                                                 .Select(p => mapper.Map<GetPostDto>(p))
                                                 .ToListAsync();
    
            return !result.Any() ? throw new ApplicationException(Exceptions.EntitiesNotFound) : result.OrderByDescending(p => p.CreatedOn);
        }

        public async Task<IEnumerable<GetPostDto>> GetPostByTitleAsync(string title)
        {
            var result = await this.context.Posts.Where(p => p.Title.ToLower().Contains(title.ToLower()))
                                               .Include(p => p.User)
                                               .Include(p => p.Category)
                                               .Include(p => p.Likes)
                                               .ToListAsync();

            var dtoResult = result.Select(post => mapper.Map<GetPostDto>
             (post, opt => opt.AfterMap((src, dest) => dest.LikesCount = post.Likes.Count))).ToList();

            return !dtoResult.Any() ? throw new ApplicationException(Exceptions.EntitiesNotFound) : dtoResult.OrderByDescending(p => p.CreatedOn);
        }

        public async Task<IEnumerable<GetPostDto>> GetPostsByCategoryAsync(int categoryId)
        {
            var category = this.context.Categories.FirstOrDefault(c => c.Id == categoryId) ?? throw new ApplicationException(Exceptions.EntityNotFound);

            var posts = await this.context.Posts
                .Where(p => p.CategoryId == categoryId)
                .ToListAsync();

            var postDtos = posts.Select(post => mapper.Map<GetPostDto>
              (post, opt => opt.AfterMap((src, dest) => dest.LikesCount = GetLikesCount(post.Id))));

            return !postDtos.Any() ? throw new ApplicationException(Exceptions.EntitiesNotFound) : postDtos;
        }

        public async Task<IEnumerable<GetPostDto>> GetPostsByCategoryAsync(string type)
        {
            var result = await this.context.Categories.Where(c => c.Type.Equals(type))
                                                      .Select(p => mapper.Map<GetPostDto>(p))
                                                      .ToListAsync();

            var postDtos = result.Select(post => mapper.Map<GetPostDto>
             (post, opt => opt.AfterMap((src, dest) => dest.LikesCount = GetLikesCount(post.Id))));

            return !postDtos.Any() ? throw new ApplicationException(Exceptions.EntitiesNotFound) : postDtos;
        }

        public async Task<IEnumerable<GetPostDto>> GetAllAsync()
        {
            var posts = await this.context.Posts
                .Include(p => p.Category)
                .Include(p => p.User)
                .Select(p => mapper.Map<GetPostDto>(p)).ToListAsync();

            return !posts.Any() ? throw new ApplicationException(Exceptions.EntitiesNotFound) : posts;
        }

        public async Task<UpdatePostDto> UpdatePostAsync(int postId, UpdatePostDto postDto)
        {
            var postModel = await this.context.Posts.FirstOrDefaultAsync(p => p.Id == postId)
                ?? throw new ApplicationException(Exceptions.EntityNotFound);

            if (postDto.Title != null)
            {
                postModel.Title = postDto.Title;
            }
            if (postDto.Content != null)
            {
                postModel.Content = postDto.Content;
            }
    
            postModel.Title = postDto.Title;
            postModel.Content = postDto.Content;
            postModel.ModifiedOn = DateTime.UtcNow;

            await this.context.SaveChangesAsync();

            return this.mapper.Map<UpdatePostDto>(postModel);
        } 

        public async Task DeletePostAsync(User user, int postId)
        {
            if (user.Role != UserRole.Admin.ToString() && !CheckIfPostIsOwnedByUser(user, postId))
            {
                throw new ApplicationException("You do not have permission to access!");
            }

            var post = await this.context.Posts.FirstOrDefaultAsync(p => p.Id == postId) ??
                                    throw new ApplicationException(Exceptions.EntityNotFound);

            this.context.Posts.Remove(post);

            await this.context.SaveChangesAsync();
        }

        public int GetLikesCount(int postId)
        {
            var likesCount = this.context.Posts
                .Where(p => p.Id == postId)
                .Include(p => p.Likes)
                .Select(p => p.Likes)
                .Count();

            return likesCount;
        }

        public async Task<int> GetPostsCount()
        {
            return await this.context.Posts.CountAsync();

        }

        public bool CheckIfPostIsOwnedByUser(User user, int postId) => user.Posts.Any(p => p.Id == postId);
    }
}