﻿using Microsoft.EntityFrameworkCore;
using PetForum.Data.Database;
using PetForum.Data.Models;
using PetForum.Service.Contracts;
using System.Linq;
using System.Threading.Tasks;

namespace PetForum.Service.Services
{
    public class UserAuthService : IUserAuthService
    {
        private readonly PetForumContext context;

        public UserAuthService(PetForumContext context)
        {
            this.context = context;
        }

        public async Task<User> LogIn(string email)
        {
                var user = await this.context.Users
                            .Where(u => u.Email == email)
                            .Include(u => u.Posts)
                            .Include(u => u.Comments)
                            .FirstOrDefaultAsync();

                return user;
        }
    }
}