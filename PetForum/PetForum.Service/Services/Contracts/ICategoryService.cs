﻿using PetForum.Service.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PetForum.Service.Services.Contracts
{
    public interface ICategoryService
    {
        Task<CategoryDto> CreateAsync(CategoryDto category);

        Task<CategoryDto> GetCategoryByIdAsync(int id);

        Task<IEnumerable<CategoryDto>> GetCategoryByTypeAsync(string keyword);

        Task<IEnumerable<CategoryDto>> GetAllAsync();

        Task<CategoryDto> UpdateCategoryAsync(int categoryId, CategoryDto CategoryDto);

        Task DeleteCategoryAsync(int id);
    }
}