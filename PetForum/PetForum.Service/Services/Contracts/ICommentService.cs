﻿using PetForum.Service.DTOs;
using PetForum.Service.DTOs.Comment;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PetForum.Service.Services
{
    public interface ICommentService
    {
        Task<CreateCommentDto> CreateCommentAsync(CreateCommentDto commentDto);

        Task CreateCommentAsync(int postId, int userId, string NewComment);

        Task<GetCommentDto> GetCommentByIdAsync(int id);

        Task<IEnumerable<GetCommentDto>> GetAllCommentsAsync();

        Task<UpdateCommentDto> UpdateCommentAsync(int id, UpdateCommentDto commentDto);

        Task DeleteCommentAsync(int id);
    }
}