﻿using PetForum.Data.Models;
using System.Threading.Tasks;

namespace PetForum.Service.Contracts
{
    public interface IUserAuthService
    {
        Task<User> LogIn(string email);
    }
}