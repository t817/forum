﻿using PetForum.Data.Models;
using PetForum.Service.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PetForum.Service.Services.Contracts
{
    public interface IPostService
    {
        Task<CreatePostDto> CreatePostAsync(CreatePostDto post);

        Task<GetPostDto> GetPostByIdAsync(int id);

        Task<IEnumerable<GetPostDto>> GetUserPostsAsync(int userId);

        Task<IEnumerable<GetPostDto>> GetPostByContentAsync(string keyword);

        Task<IEnumerable<GetPostDto>> GetPostByTitleAsync(string title);

        Task<IEnumerable<GetPostDto>> GetPostsByCategoryAsync(int categoryId);

        Task<IEnumerable<GetPostDto>> GetPostsByCategoryAsync(string type);

        Task<IEnumerable<GetPostDto>> GetAllAsync();

        Task<UpdatePostDto> UpdatePostAsync(int postId, UpdatePostDto postDto);

        Task DeletePostAsync(User user, int postId);

        int GetLikesCount(int postId);

        Task<int> GetPostsCount();
    }
}