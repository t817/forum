﻿using PetForum.Service.DTOs.User;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PetForum.Service.Services.Contracts
{
    public interface IUserService
    {
        Task<GetUserDto> GetUserByIdAsync(int id);

        Task<GetUserDto> GetUserByUsernameAsync(string username);

        Task<GetUserDto> GetUserByEmailAsync(string email);

        Task<IEnumerable<GetUserDto>> GetUsersByDisplayNameAsync(string displayName);

        Task<IEnumerable<GetUserDto>> GetAllUsersAsync();

        Task<CreateUserDto> CreateUserAsync(CreateUserDto input);

        Task<UpdateUserDto> UpdateUserAsync(int id, UpdateUserDto userUpdateDto);

        Task DeleteUserAsync(int id);

        Task<bool> BlockUserAsync(int id);

        Task<bool> UnblockUserAsync(int id);

        Task<int> GetUsersCount();
    }
}