﻿using PetForum.Service.DTOs;
using System.Threading.Tasks;

namespace PetForum.Service.Services.Contracts
{
    public interface INotificationService
    {
        Task<NotificationDto> CreateNotificationAsync(NotificationDto notificationDto);
    }
}