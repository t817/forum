﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PetForum.Data.Database;
using PetForum.Data.Models;
using PetForum.Service.DTOs;
using PetForum.Service.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetForum.Service.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly PetForumContext context;
        private readonly IMapper mapper;

        public CategoryService(PetForumContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<CategoryDto> CreateAsync(CategoryDto categoryDto)
        {
            var category = await this.context.Categories
              .FirstOrDefaultAsync(p => p.Type == categoryDto.Type);

            if (category != null)
            {
                throw new ApplicationException($"Category with type {categoryDto.Type} already exists.");
            }
            else
            {
                var newCategory = this.mapper.Map<Category>(categoryDto);

                await this.context.Categories.AddAsync(newCategory);

                await this.context.SaveChangesAsync();

                return this.mapper.Map<CategoryDto>(newCategory);
            }
        }

        public async Task<CategoryDto> GetCategoryByIdAsync(int id)
        {
            var category = await this.context.Categories.FirstOrDefaultAsync(c => c.Id == id)
               
               ?? throw new ApplicationException(Exceptions.EntityNotFound);

            return this.mapper.Map<CategoryDto>(category);
        }

        public async Task<IEnumerable<CategoryDto>> GetCategoryByTypeAsync(string keyword)
        {
            var result = await this.context.Categories.Where(c => c.Type.Contains(keyword))
                                                 .Select(c => mapper.Map<CategoryDto>(c))
                                                 .ToListAsync();

            return !result.Any() ? throw new ApplicationException(Exceptions.EntitiesNotFound) : result.OrderByDescending(c => c.CreatedOn);
        }

        public async Task<IEnumerable<CategoryDto>> GetAllAsync()
        {
            var categories = await this.context.Categories.Select(c => mapper.Map<CategoryDto>(c)).ToListAsync();

            return categories.Count == 0 ? throw new ApplicationException(Exceptions.EntitiesNotFound) : categories;
        }

        public async Task<CategoryDto> UpdateCategoryAsync(int categoryId, CategoryDto categoryDto)
        {
            var categoryModel = await this.context.Categories.FirstOrDefaultAsync(p => p.Id == categoryId)
                ?? throw new ApplicationException(Exceptions.EntityNotFound);

            if (categoryDto.Type != null)
            {
                categoryModel.Type = categoryDto.Type;
            }

            categoryModel.Type = categoryDto.Type;
            categoryModel.ModifiedOn = DateTime.UtcNow;

            await this.context.SaveChangesAsync();

            return this.mapper.Map<CategoryDto>(categoryModel);
        }

        public async Task DeleteCategoryAsync(int id)
        {
            var category = await this.context.Categories.FirstOrDefaultAsync(c => c.Id == id)
            ?? throw new ApplicationException(Exceptions.EntityNotFound);

            this.context.Categories.Remove(category);
            await this.context.SaveChangesAsync();
        }    
    }
}