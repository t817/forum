﻿namespace PetForum.Service
{
    public class Exceptions
    {
        public const string EntityNotFound = "Entity is not found.";
        public const string EntitiesNotFound = "Entities are not found.";

        public const string InvalidModel = "Model is not valid.";
        public const string InvalidCriteria = "You have entered an invalid criteria.";
        public const string SomethingWentWrong = "Something went wrong.";

        public const string InvalidFile = "Invalid file provided.";
        public const string Error = "Something went wrong. Please, try again.";
    }
}