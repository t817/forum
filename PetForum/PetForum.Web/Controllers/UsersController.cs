﻿using Microsoft.AspNetCore.Mvc;
using PetForum.Data.Enums;
using PetForum.Data.Models;
using PetForum.Service.Contracts;
using PetForum.Service.DTOs.User;
using PetForum.Service.Services.Contracts;
using PetForum.Web.Controllers.Abstract;
using System;
using System.Threading.Tasks;

namespace PetForum.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : BaseController
    {
        private readonly IUserService userService;
        public UsersController(IUserService userService, IUserAuthService identity)
            : base(identity)
        {
            this.userService = userService;
        }

        /// <summary>
        /// Get user by id
        /// </summary>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get([FromHeader] string email, int id)
        {
            try
            {
                User user = await this .LogIn(email);

                if (user == null)
                {
                    return this.BadRequest($"There is no user with email: {email}!");
                }
                if (user.Role == UserRole.Member.ToString())
                {
                    return this.Unauthorized("You do not have permission to access!");
                }

                var result = await this.userService.GetUserByIdAsync(id);

                return this.Ok(result);
            }
            catch (Exception e)
            {
                return this.NotFound(e.Message);
            }
        }

        /// <summary>
        /// Get user by username or get all users
        /// </summary>
        [HttpGet("")]
        public async Task<IActionResult> Get([FromQuery] string username)
        {
            try
            {
                if (username == null)
                {
                    var users = await this.userService.GetAllUsersAsync();
                    return this.Ok(users);
                }
                else
                {
                    var user = await this.userService.GetUserByUsernameAsync(username);
                    return this.Ok(user);
                }
            }
            catch (ApplicationException e)
            {
                return this.NotFound(e.Message);
            }
        }

        /// <summary>
        /// Create user
        /// </summary>
        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] CreateUserDto input)
        {
            try
            {
                var user = await this.userService.CreateUserAsync(input);
                return this.Created($"api/users/info?username={user.Username}", user); 
            }
            catch (ApplicationException e)
            {
                return this.BadRequest(e.Message);
            }
        }

        /// <summary>
        /// User can update his information
        /// </summary>
        [HttpPut("personal")]
        public async Task<IActionResult> Put([FromHeader] string email, [FromQuery] UpdateUserDto parameters)
        {
            User user = await this.LogIn(email);
            if (user == null)
            {
                return this.BadRequest($"There is no user with email: {email}!");
            }
            if (user.Role == UserRole.Admin.ToString())
            {
                return this.Unauthorized("You do not have permission to access!");
            }

            try
            {
                var result = await this.userService.UpdateUserAsync(user.Id, parameters);

                return this.Ok(result);
            }
            catch (ApplicationException e)
            {
                return this.BadRequest(e.Message);
            }
        }

        /// <summary>
        /// User can delete himself from database
        /// </summary>
        [HttpDelete("")]
        public async Task<IActionResult> Delete([FromHeader] string email)
        {
            User user = await this.LogIn(email);

            if (user == null)
            {
                return this.BadRequest($"There is no user with email: {email}!");
            }
            if (user.Role == UserRole.Admin.ToString())
            {
                return this.Unauthorized("You do not have permission to access!");
            }

            try
            {
                await this.userService.DeleteUserAsync(user.Id);
                return this.NoContent();
            }
            catch (ApplicationException e)
            {
                this.HttpContext.Response.StatusCode = 400;
                return this.BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Admin can delete member from database
        /// </summary>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromHeader] string email, int id)
        {
            User user = await this .LogIn(email);

            if (user == null)
            {
                return this.BadRequest($"There is no user with email: {email}!");
            }
            if (user.Role == UserRole.Member.ToString())
            {
                return this.Unauthorized("You do not have permission to access!");
            }

            try
            {
                await this.userService.DeleteUserAsync(id);
                return this.NoContent();
            }
            catch (ApplicationException ae)
            {
                this.HttpContext.Response.StatusCode = 400;
                return this.BadRequest(ae.Message);
            }
        }

        /// <summary>
        /// Admin can block member
        /// </summary>
        [HttpGet("{id}/block")]
        public async Task<IActionResult> BlockUser([FromHeader] string email, int id)
        {
            User user = await this .LogIn(email);

            if (user == null)
            {
                return this.BadRequest($"There is no user with email: {email}!");
            }
            if (user.Role == UserRole.Member.ToString())
            {
                return this.Unauthorized("You do not have permission to access!");
            }

            try
            {
                await this.userService.BlockUserAsync(id);

                return this.NoContent();
            }
            catch (Exception e)
            {
                this.HttpContext.Response.StatusCode = 400;
                return this.BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Admin can unblock member
        /// </summary>
        [HttpGet("{id}/unblock")]
        public async Task<IActionResult> UnblockUser([FromHeader] string email, int id)
        {
            User user = await this.LogIn(email);

            if (user == null)
            {
                return this.BadRequest($"There is no user with email: {email}!");
            }
            if (user.Role == UserRole.Member.ToString())
            {
                return this.Unauthorized("You do not have permission to access!");
            }

            try
            {
                await this.userService.UnblockUserAsync(id);

                return this.NoContent();
            }
            catch (Exception e)
            {
                this.HttpContext.Response.StatusCode = 400;
                return this.BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Admin can search member by username, email or displayname
        /// </summary>
        [HttpGet("search")]
        public async Task<IActionResult> SearchBy ([FromHeader] string email, [FromQuery] SearchUserByDto searchBy)
        {
            User user = await this.LogIn(email);

            if (user == null)
            {
                return this.BadRequest($"There is no user with email: {email}!");
            }
            if (user.Role == UserRole.Member.ToString())
            {
                return this.Unauthorized("You do not have permission to access!");
            }

            try
            {
                if (searchBy.Username != null)
                {
                    var searchedUser = await this.userService.GetUserByUsernameAsync(searchBy.Username);
                    return this.Ok(searchedUser);
                }
                else if (searchBy.Email != null)
                {
                    var searchedUser = await this.userService.GetUserByEmailAsync(searchBy.Email);
                    return this.Ok(searchedUser);
                }
                else
                {
                    var searchedUser = await this.userService.GetUsersByDisplayNameAsync(searchBy.DisplayName);
                    return this.Ok(searchedUser);
                }
            }
            catch (ApplicationException e)
            {
                return this.NotFound(e.Message);
            }
        }
    }
}