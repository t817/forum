﻿using Microsoft.AspNetCore.Mvc;
using PetForum.Data.Models;
using PetForum.Service.Contracts;
using System.Threading.Tasks;

namespace PetForum.Web.Controllers.Abstract
{
    public abstract class BaseController : ControllerBase
    {
        private readonly IUserAuthService service;

        protected BaseController(IUserAuthService service)
        {
            this.service = service;
        }

        protected async Task<User> LogIn(string email)
        {
            return await this.service.LogIn(email);
        }
    }
}