﻿using Microsoft.AspNetCore.Mvc;
using PetForum.Data.Enums;
using PetForum.Data.Models;
using PetForum.Service.Contracts;
using PetForum.Service.DTOs;
using PetForum.Service.DTOs.Comment;
using PetForum.Service.Enums;
using PetForum.Service.Services;
using PetForum.Service.Services.Contracts;
using PetForum.Web.Controllers.Abstract;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PetForum.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PostsController : BaseController
    {
        private readonly IPostService postService;
        private readonly ICommentService commentService;

        public PostsController(IPostService postService, IUserAuthService identity, ICommentService commentService)
            : base(identity)
        {
            this.postService = postService;
            this.commentService = commentService;
        }

        /// <summary>
        /// Get post by id
        /// </summary>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get([FromHeader] string email, int id)
        {
            User user = await this.LogIn(email);

            if (user == null)
            {
                return this.BadRequest($"There is no user with email: {email}!");
            }
            if (user.Role == UserRole.Member.ToString())
            {
                return this.Unauthorized("You do not have permission to access!");
            }

            try
            {
                var post = await this.postService.GetPostByIdAsync(id);

                return this.Ok(post);
            }
            catch (Exception e)
            {
                return this.NotFound(e.Message);
            }
        }

        /// <summary>
        /// Get post by title or get all posts
        /// </summary>
        [HttpGet("")]
        public async Task<IActionResult> Get([FromQuery] string title)
        {
            try
            {
                if (title == null)
                {
                    var posts = await this.postService.GetAllAsync();
                    return this.Ok(posts);
                }
                else
                {
                    var post = await this.postService.GetPostByTitleAsync(title);
                    return this.Ok(post);
                }
            }
            catch (ApplicationException e)
            {
                return this.NotFound(e.Message);
            }
        }

        /// <summary>
        /// Create post
        /// </summary>
        [HttpPost("")]
        public async Task<IActionResult> Post([FromHeader] string email, [FromBody] CreatePostDto input)
        {
            User user = await this.LogIn(email);

            if (user == null)
            {
                return this.BadRequest($"There is no user with email: {email}!");
            }

            try
            {
                var post = await this.postService.CreatePostAsync(input);
                return this.Created($"api/posts/info?title={post.Title}", post);
            }
            catch (ApplicationException e)
            {
                return this.BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Admin can delete any post and Member can delete his post
        /// </summary>
        [HttpDelete("{postId}")]
        public async Task<IActionResult> Delete([FromHeader] string email, int postId)
        {
            User user = await this .LogIn(email);

            if (user == null)
            {
                return this.BadRequest($"There is no user with email: {email}!");
            }

            try
            {
                await this.postService.DeletePostAsync(user, postId);
                return this.NoContent();
            }
            catch (ApplicationException e)
            {
                this.HttpContext.Response.StatusCode = 400;
                return this.BadRequest(e.Message);
            }
        }

        /// <summary>
        /// User can add comment to any post
        /// </summary>
        [HttpPost("{id}/comments")]
        public async Task<IActionResult> Comment([FromHeader] string email, [FromBody] CreateCommentDto comment)
        {
            User user = await this .LogIn(email);

            if (user == null)
            {
                return this.BadRequest($"There is no user with email: {email}!");
            }

            try
            {
                await this.commentService.CreateCommentAsync(comment);
                return this.Ok(comment);
            }
            catch (ApplicationException e)
            {
                this.HttpContext.Response.StatusCode = 400;
                return this.BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Admin can delete any comment and Member can delete his comment
        /// </summary>
        [HttpDelete("{id}/comments/{commentId}")]
        public async Task<IActionResult> DeleteComment([FromHeader] string email, int commentId)
        {
            var comment = await this.commentService.GetCommentByIdAsync(commentId);

            if (comment == null)
            {
                throw new Exception($"There is no comment with id: {commentId}!");
            }

            User user = await this.LogIn(email);

            if (user == null)
            {
                return this.BadRequest($"There is no user with email: {email}!");
            }

            if (user.Role == UserRole.Member.ToString() && comment.UserId != user.Id)
            {
                return this.Unauthorized("You do not have permission to access!");
            }

            try
            {
                await this.commentService.DeleteCommentAsync(commentId);
                return this.NoContent();
            }
            catch (ApplicationException e)
            {
                this.HttpContext.Response.StatusCode = 400;
                return this.BadRequest(e.Message);
            }
        }

        /// <summary>
        /// User can update his comment
        /// </summary>
        [HttpPut("{id}/comments/{commentId}")]
        public async Task<IActionResult> UpdateComment([FromHeader] string email, [FromBody] UpdateCommentDto parameters, int commentId)
        {
            var comment = await this.commentService.GetCommentByIdAsync(commentId);


            User user = await this .LogIn(email);
            if (user == null)
            {
                return this.BadRequest($"There is no user with email: {email}!");
            }

            if (comment.UserId != user.Id)
            {
                return this.Unauthorized("You do not have permission to access!");
            }

            try
            {
                var result = await this.commentService.UpdateCommentAsync(commentId, parameters);
                return this.Ok(result);
            }
            catch (ApplicationException e)
            {
                this.HttpContext.Response.StatusCode = 400;
                return this.BadRequest(e.Message);
            }
        }

        /// <summary>
        /// User can sort posts
        /// </summary>
        [HttpGet("sort")]
        public async Task<IActionResult> SortPostsAsync([FromHeader] string email, PostsSort sort)
        {

            User user = await this .LogIn(email);
            if (user == null)
            {
                return this.BadRequest($"There is no user with email: {email}!");
            }

            var posts = await this.postService.GetAllAsync();

            if (sort == PostsSort.CreatedOn)
            {
                posts = posts.OrderByDescending(p => p.CreatedOn);
            }
            else if (sort == PostsSort.ModifiedOn)
            {
                posts = posts.OrderByDescending(p => p.ModifiedOn);
            }
            else if (sort == PostsSort.Category)
            {
                posts = posts.OrderBy(p => p.Category);
            }

            return this.Ok(posts);
        }
    }
}