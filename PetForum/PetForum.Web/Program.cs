using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PetForum.Data.Database;
using System.Threading.Tasks;

namespace PetForum.Web
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();            
            
            using var scope = host.Services.CreateScope();
            var context = scope.ServiceProvider.GetService<PetForumContext>();

            await context.EnsureSeeded();

            await host.RunAsync();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
         Host.CreateDefaultBuilder(args)
             .ConfigureWebHostDefaults(webBuilder =>
             {
                 webBuilder.UseStartup<Startup>();
             });
    }
}