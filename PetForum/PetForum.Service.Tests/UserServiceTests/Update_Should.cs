﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PetForum.Data.Database;
using PetForum.Data.Models;
using PetForum.Service;
using PetForum.Service.DTOs.User;
using PetForum.Service.ModelMapper;
using PetForum.Service.Services;
using PetForum.Service.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace PetForum.Tests.UserServiceTests
{
    [TestClass]
    public class Update_Should
    {
        private readonly IMapper mapper;
        public Update_Should()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile(new AutoMapping()); });
            this.mapper = mapperConfiguration.CreateMapper();
        }

        [TestMethod]
        public async Task UpdateUser_Correctly()
        {
            int testUserId = 1;

            var mockUserService = new Mock<IUserService>().Object;

            var options = Utils.GetOptions(nameof(UpdateUser_Correctly));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Users.Add(new User { Username = "TestUser1", Password = "Abc23kc*r", Email = "test1@abv.bg", DisplayName = "TestU1", FirstName = "Kiril", LastName = "Petkov", Role = "Member" });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var updateUserDto = new UpdateUserDto()
                {
                    Username = "NewUsername",
                    Email = "NewEmail",
                    DisplayName = "NewDisplayName",
                    FirstName = "NewFirstName",
                    LastName = "NewLastName",
                    ModifiedOn = DateTime.Now
                };

                var sut = new UserService(assertContext, mapper);
                var updatedUser = await sut.UpdateUserAsync(testUserId, updateUserDto);
                Assert.AreEqual("NewUsername", updatedUser.Username);
                Assert.AreEqual("NewEmail", updatedUser.Email);
                Assert.AreEqual("NewDisplayName", updatedUser.DisplayName);
                Assert.AreEqual("NewFirstName", updatedUser.FirstName);
                Assert.AreEqual("NewLastName", updatedUser.LastName);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntityNotFound)]
        public async Task ThrowWneh_IdDoesNotExist()
        {
            int testUserId = 3;

            var mockUserService = new Mock<IUserService>().Object;

            var options = Utils.GetOptions(nameof(ThrowWneh_IdDoesNotExist));

            var config = Utils.GetMappingConfig();

            using (var assertContext = new PetForumContext(options))
            {
                var updateUserDto = new UpdateUserDto()
                {
                    Username = "NewUsername",
                    Email = "NewEmail",
                    DisplayName = "NewDisplayName",
                    FirstName = "NewFirstName",
                    LastName = "NewLastName",
                    ModifiedOn = DateTime.Now
                };

                var sut = new UserService(assertContext, mapper);
                var user = await sut.UpdateUserAsync(testUserId, updateUserDto);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task ThrowWneh_EmailIsNotAllowed()
        {
            int testUserId = 1;

            var mockUserService = new Mock<IUserService>().Object;

            var options = Utils.GetOptions(nameof(ThrowWneh_EmailIsNotAllowed));

            var config = Utils.GetMappingConfig();


            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Users.Add(new User { Username = "TestUser1", Password = "Abc23kc*r", Email = "test1@abv.bg", DisplayName = "TestU1", FirstName = "Kiril", LastName = "Petkov", Role = "Member" });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var updateUserDto = new UpdateUserDto()
                {
                    Username = "NewUsername",
                    Email = "newemail@petforum.com",
                    DisplayName = "NewDisplayName",
                    FirstName = "NewFirstName",
                    LastName = "NewLastName",
                    ModifiedOn = DateTime.Now
                };

                var sut = new UserService(assertContext, mapper);
                var user = await sut.UpdateUserAsync(testUserId, updateUserDto);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task ThrowWneh_EmailAlreadyExists()
        {
            int testUserId = 1;

            var mockUserService = new Mock<IUserService>().Object;

            var options = Utils.GetOptions(nameof(ThrowWneh_EmailAlreadyExists));

            var config = Utils.GetMappingConfig();


            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Users.Add(new User { Username = "TestUser1", Password = "Abc23kc*r", Email = "test1@abv.bg", DisplayName = "TestU1", FirstName = "Kiril", LastName = "Petkov", Role = "Member" });

                arrangeContext.Users.Add(new User { Username = "TestUser2", Password = "Abc2ff3kc*r", Email = "test2@abv.bg", DisplayName = "TestU2", FirstName = "Kiril", LastName = "Petkov", Role = "Member" });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var updateUserDto = new UpdateUserDto()
                {                
                    Username = "NewUsername",
                    Email = "test2@abv.bg",
                    DisplayName = "NewDisplayName",
                    FirstName = "NewFirstName",
                    LastName = "NewLastName",
                    ModifiedOn = DateTime.Now
                };

                var sut = new UserService(assertContext, mapper);
                var user = await sut.UpdateUserAsync(testUserId, updateUserDto);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), "You can not use email: @petforum.com for registration!")]
        public async Task ThrowWneh_UsernameAlreadyExists()
        {
            int testUserId = 1;

            var mockUserService = new Mock<IUserService>().Object;

            var options = Utils.GetOptions(nameof(ThrowWneh_UsernameAlreadyExists));

            var config = Utils.GetMappingConfig();


            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Users.Add(new User { Username = "TestUser1", Password = "Abc23kc*r", Email = "test1@abv.bg", DisplayName = "TestU1", FirstName = "Kiril", LastName = "Petkov", Role = "Member" });

                arrangeContext.Users.Add(new User { Username = "TestUser2", Password = "Abc636kc*jaj", Email = "test2@abv.bg", DisplayName = "TestU2", FirstName = "Ivan", LastName = "Ivanov", Role = "Member" });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var updateUserDto = new UpdateUserDto()
                {
                    Username = "TestUser2",
                    Email = "test@abv.bg",
                    DisplayName = "NewDisplayName",
                    FirstName = "NewFirstName",
                    LastName = "NewLastName",
                    ModifiedOn = DateTime.Now
                };

                var sut = new UserService(assertContext, mapper);
                var user = await sut.UpdateUserAsync(testUserId, updateUserDto);
            }
        }
    }
}