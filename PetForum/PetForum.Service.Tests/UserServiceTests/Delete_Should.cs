﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PetForum.Data.Database;
using PetForum.Data.Models;
using PetForum.Service;
using PetForum.Service.ModelMapper;
using PetForum.Service.Services;
using PetForum.Service.Services.Contracts;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PetForum.Tests.UserServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        private readonly IMapper mapper;
        public Delete_Should()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile(new AutoMapping()); });
            this.mapper = mapperConfiguration.CreateMapper();
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntitiesNotFound)]
        public async Task DeleteUser_WhenExists()
        {
            int testUserId= 1;
            int testUsersCount = 1;

            var mockUserService = new Mock<IUserService>().Object;

            var options = Utils.GetOptions(nameof(DeleteUser_WhenExists));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Users.Add(new User { Username = "TestUser1", Password = "Abc23kc*r", Email = "test1@abv.bg", DisplayName = "TestU1", FirstName = "Kiril", LastName = "Petkov", Role = "Member" });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new UserService(assertContext, mapper);
                var usersBeforeDelete = await sut.GetAllUsersAsync();
                Assert.AreEqual(testUsersCount, usersBeforeDelete.Count());
                await sut.DeleteUserAsync(testUserId);
                var usersAfterDelete = await sut.GetAllUsersAsync();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntityNotFound)]
        public async Task ThrowWhen_UserDoesNotExist()
        {
            int testUserId = 1;

            var mockUserService = new Mock<IUserService>().Object;

            var options = Utils.GetOptions(nameof(ThrowWhen_UserDoesNotExist));

            var config = Utils.GetMappingConfig();

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new UserService(assertContext, mapper);
                await sut.DeleteUserAsync(testUserId);
            }
        }
    }
}