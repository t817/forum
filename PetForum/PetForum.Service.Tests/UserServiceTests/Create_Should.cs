﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PetForum.Data.Database;
using PetForum.Data.Models;
using PetForum.Service.DTOs.User;
using PetForum.Service.ModelMapper;
using PetForum.Service.Services;
using PetForum.Service.Services.Contracts;
using System.Threading.Tasks;

namespace PetForum.Tests.UserServiceTests
{
    [TestClass]
    public class Create_Should
    {
        private readonly IMapper mapper;
        public Create_Should()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile(new AutoMapping()); });
            this.mapper = mapperConfiguration.CreateMapper();
        }

        [TestMethod]
        public async Task CreateUser()
        {
            string testUsername = "TestUser1";

            var mockUserService = new Mock<IUserService>().Object;

            var options = Utils.GetOptions(nameof(CreateUser));

            var config = Utils.GetMappingConfig();

            using (var assertContext = new PetForumContext(options))
            {
                var user = new User {Username = "TestUser1", Password = "Abc23kc*r", Email = "test1@abv.bg", DisplayName = "TestU1", FirstName = "Kiril", LastName = "Petkov", Role = "Member" };
                var createUserDto = this.mapper.Map<CreateUserDto>(user);
                var sut = new UserService(assertContext, mapper);
                var newUser = await sut.CreateUserAsync(createUserDto);
                Assert.AreEqual(testUsername, newUser.Username);
            }
        }
    }
}