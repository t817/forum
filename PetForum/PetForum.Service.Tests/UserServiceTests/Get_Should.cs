﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PetForum.Data.Database;
using PetForum.Data.Models;
using PetForum.Service;
using PetForum.Service.ModelMapper;
using PetForum.Service.Services;
using PetForum.Service.Services.Contracts;
using System;
using System.Linq;
using System.Threading.Tasks;


namespace PetForum.Tests.UserServiceTests
{
    [TestClass]
    public class Get_Should
    {
        private readonly IMapper mapper;
        public Get_Should()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile(new AutoMapping()); });
            mapper = mapperConfiguration.CreateMapper();
        }

        [TestMethod]
        public async Task GetCorrectUser_ById()
        {
            int testUserId = 1;

            var mockUserService = new Mock<IUserService>().Object;

            var options = Utils.GetOptions(nameof(GetCorrectUser_ById));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Users.Add(new User {Username = "TestUser1", Password = "Abc23kc*r", Email = "test1@abv.bg", DisplayName = "TestU1", FirstName = "Kiril", LastName = "Petkov", Role = "Member" });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new UserService(assertContext, mapper);
                var user = await sut.GetUserByIdAsync(testUserId);
                Assert.AreEqual(testUserId, user.Id);
            }
        }

        [TestMethod]
        public async Task GetCorrectUser_ByUsername()
        {
            string testUsername = "TestUser1";

            var mockUserService = new Mock<IUserService>().Object;

            var options = Utils.GetOptions(nameof(GetCorrectUser_ByUsername));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Users.Add(new User { Username = "TestUser1", Password = "Abc23kc*r", Email = "test1@abv.bg", DisplayName = "TestU1", FirstName = "Kiril", LastName = "Petkov", Role = "Member" });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new UserService(assertContext, mapper);
                var user = await sut.GetUserByUsernameAsync(testUsername);
                Assert.AreEqual(testUsername, user.Username);
            }
        }

        [TestMethod]
        public async Task GetCorrectUser_ByEmail()
        {
            string testEmail = "test1@abv.bg";

            var mockUserService = new Mock<IUserService>().Object;

            var options = Utils.GetOptions(nameof(GetCorrectUser_ByUsername));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Users.Add(new User { Username = "TestUser1", Password = "Abc23kc*r", Email = "test1@abv.bg", DisplayName = "TestU1", FirstName = "Kiril", LastName = "Petkov", Role = "Member" });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new UserService(assertContext, mapper);
                var user = await sut.GetUserByEmailAsync(testEmail);
                Assert.AreEqual(testEmail, user.Email);
            }
        }

        [TestMethod]
        public async Task GetCorrectUser_ByDisplayName()
        {
            string testDisplayName = "TestU1";
            int testCount = 1;

            var mockUserService = new Mock<IUserService>().Object;

            var options = Utils.GetOptions(nameof(GetCorrectUser_ByDisplayName));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Users.Add(new User { Username = "TestUser1", Password = "Abc23kc*r", Email = "test1@abv.bg", DisplayName = "TestU1", FirstName = "Kiril", LastName = "Petkov", Role = "Member" });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new UserService(assertContext, mapper);
                var users = await sut.GetUsersByDisplayNameAsync(testDisplayName);
                Assert.AreEqual(testCount, users.Count());
            }
        }

        [TestMethod]
        public async Task GetAllUsers()
        {
            int testCount = 2;

            var mockUserService = new Mock<IUserService>().Object;

            var options = Utils.GetOptions(nameof(GetAllUsers));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Users.Add(new User { Username = "TestUser1", Password = "Abc23kc*r", Email = "test1@abv.bg", DisplayName = "TestU1", FirstName = "Kiril", LastName = "Petkov", Role = "Member" });
                arrangeContext.Users.Add(new User { Username = "TestUser2", Password = "Abc23khaha&", Email = "test2@abv.bg", DisplayName = "TestU2", FirstName = "Ivan", LastName = "Ivanov", Role = "Member" });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new UserService(assertContext, mapper);
                var users = await sut.GetAllUsersAsync();
                Assert.AreEqual(testCount, users.Count());
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntityNotFound)]
        public async Task ThrowWhen_UsernameDoesNotExist()
        {
            string testUsername = "TestName2";
            var mockUserService = new Mock<IUserService>().Object;
            var mockMapper = new Mock<IMapper>().Object;
            var options = Utils.GetOptions(nameof(ThrowWhen_UsernameDoesNotExist));

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new UserService(assertContext, mockMapper);
                var user = await sut.GetUserByUsernameAsync(testUsername);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntityNotFound)]
        public async Task ThrowWneh_IdDoesNotExist()
        {
            int testUserId = 3;

            var mockUserService = new Mock<IUserService>().Object;

            var options = Utils.GetOptions(nameof(ThrowWneh_IdDoesNotExist));

            var config = Utils.GetMappingConfig();

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new UserService(assertContext, mapper);
                var user = await sut.GetUserByIdAsync(testUserId);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntityNotFound)]
        public async Task ThrowWhen_EmailDoesNotExist()
        {
            string testEmail = "test@gmail.com";
            var mockUserService = new Mock<IUserService>().Object;
            var mockMapper = new Mock<IMapper>().Object;
            var options = Utils.GetOptions(nameof(ThrowWhen_EmailDoesNotExist));

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new UserService(assertContext, mockMapper);
                var user = await sut.GetUserByEmailAsync(testEmail);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntitiesNotFound)]
        public async Task ThrowWhen_DisplaynameDoesNotExist()
        {
            string testDisplayName = "TestDisplayName";
            var mockUserService = new Mock<IUserService>().Object;
            var mockMapper = new Mock<IMapper>().Object;
            var options = Utils.GetOptions(nameof(ThrowWhen_DisplaynameDoesNotExist));

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new UserService(assertContext, mockMapper);
                var user = await sut.GetUsersByDisplayNameAsync(testDisplayName);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntitiesNotFound)]
        public async Task ThrowWhen_NoUsersFound()
        {
            var mockUserService = new Mock<IUserService>().Object;
            var mockMapper = new Mock<IMapper>().Object;
            var options = Utils.GetOptions(nameof(ThrowWhen_NoUsersFound));

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new UserService(assertContext, mockMapper);
                var users = await sut.GetAllUsersAsync();
            }
        }
    }
}