﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PetForum.Data.Database;
using PetForum.Data.Models;
using PetForum.Service;
using PetForum.Service.ModelMapper;
using PetForum.Service.Services;
using PetForum.Service.Services.Contracts;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PetForum.Tests.CategoryServiceTests
{
    [TestClass]
    public class Get_Should
    {
        private readonly IMapper mapper;
        public Get_Should()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile(new AutoMapping()); });
            mapper = mapperConfiguration.CreateMapper();
        }

        [TestMethod]
        public async Task GetCorrectCategory_ById()
        {
            int testCategoryId = 1;

            var mockCategoryService = new Mock<ICategoryService>().Object;

            var options = Utils.GetOptions(nameof(GetCorrectCategory_ById));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Categories.Add(new Category { Type = "Dogs" });

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new CategoryService(assertContext, mapper);
                var category = await sut.GetCategoryByIdAsync(testCategoryId);
                Assert.AreEqual(testCategoryId, category.Id);
            }
        }

        [TestMethod]
        public async Task GetAllCategories()
        {
            int testCount = 2;

            var mockCategoryService = new Mock<ICategoryService>().Object;

            var options = Utils.GetOptions(nameof(GetAllCategories));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Categories.Add(new Category { Type = "Dogs" });

                arrangeContext.Categories.Add(new Category { Type = "Dogs" });

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new CategoryService(assertContext, mapper);
                var category = await sut.GetAllAsync();
                Assert.AreEqual(testCount, category.Count());
            }
        }

        [TestMethod]
        public async Task GetCorrectCategory_ByType()
        {
            string testType = "for dogs";

            int testCount = 2;

            var mockCategoryService = new Mock<ICategoryService>().Object;

            var options = Utils.GetOptions(nameof(GetCorrectCategory_ByType));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Categories.Add(new Category { Type = "Category type for dogs" });

                arrangeContext.Categories.Add(new Category { Type = "Category type for dogs and cats" });

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new CategoryService(assertContext, mapper);
                var categories = await sut.GetCategoryByTypeAsync(testType);
                Assert.AreEqual(testCount, categories.Count());
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntityNotFound)]
        public async Task ThrowWhen_IdDoesNotExist()
        {
            int testCategoryId = 3;

            var mockCategoryService = new Mock<ICategoryService>().Object;

            var options = Utils.GetOptions(nameof(ThrowWhen_IdDoesNotExist));

            var config = Utils.GetMappingConfig();

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new CategoryService(assertContext, mapper);
                var category = await sut.GetCategoryByIdAsync(testCategoryId);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntitiesNotFound)]
        public async Task ThrowWhen_NoCategoriesFound()
        {
            var mockCategoryService = new Mock<ICategoryService>().Object;
            var mockMapper = new Mock<IMapper>().Object;
            var options = Utils.GetOptions(nameof(ThrowWhen_NoCategoriesFound));

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new CategoryService(assertContext, mockMapper);
                var categories = await sut.GetAllAsync();
            }
        }
    }
}