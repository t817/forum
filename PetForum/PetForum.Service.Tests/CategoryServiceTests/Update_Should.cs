﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PetForum.Data.Database;
using PetForum.Data.Models;
using PetForum.Service;
using PetForum.Service.DTOs;
using PetForum.Service.ModelMapper;
using PetForum.Service.Services;
using PetForum.Service.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace PetForum.Tests.CategoryServiceTests
{
    [TestClass]
    public class Update_Should
    {
        private readonly IMapper mapper;
        public Update_Should()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile(new AutoMapping()); });
            this.mapper = mapperConfiguration.CreateMapper();
        }

        [TestMethod]
        public async Task UpdateCategory_Correctly()
        {
            int testCategoryId = 1;

            var mockCategoryService = new Mock<ICategoryService>().Object;

            var options = Utils.GetOptions(nameof(UpdateCategory_Correctly));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Categories.Add(new Category { Type = "TestType"});

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var updateCategoryDto = new CategoryDto()
                {
                    Type = "NewType",
                    ModifiedOn = DateTime.Now
                };

                var sut = new CategoryService(assertContext, mapper);
                var updatedCategory = await sut.UpdateCategoryAsync(testCategoryId, updateCategoryDto);
                Assert.AreEqual("NewType", updateCategoryDto.Type);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntityNotFound)]
        public async Task ThrowWhen_IdDoesNotExist()
        {
            int testCategoryId = 3;

            var mockCategoryService = new Mock<ICategoryService>().Object;

            var options = Utils.GetOptions(nameof(ThrowWhen_IdDoesNotExist));

            var config = Utils.GetMappingConfig();

            using (var assertContext = new PetForumContext(options))
            {
                var updateCategoryDto = new CategoryDto()
                {
                    Type = "NewType",
                    ModifiedOn = DateTime.Now
                };

                var sut = new CategoryService(assertContext, mapper);
                var category = await sut.UpdateCategoryAsync(testCategoryId, updateCategoryDto);
            }
        }
    }
}