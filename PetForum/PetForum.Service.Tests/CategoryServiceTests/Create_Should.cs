﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PetForum.Data.Database;
using PetForum.Data.Models;
using PetForum.Service.DTOs;
using PetForum.Service.ModelMapper;
using PetForum.Service.Services;
using PetForum.Service.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace PetForum.Tests.CategoryServiceTests
{
    [TestClass]
    public class Create_Should
    {
        private readonly IMapper mapper;
        public Create_Should()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile(new AutoMapping()); });
            this.mapper = mapperConfiguration.CreateMapper();
        }

        [TestMethod]
        public async Task CreateCategory()
        {
            string testType = "Test type";

            int testCategoryId = 1;

            var mockCategoryService = new Mock<ICategoryService>().Object;

            var options = Utils.GetOptions(nameof(CreateCategory));

            var config = Utils.GetMappingConfig();      

            using (var assertContext = new PetForumContext(options))
            {
                var category = new Category { Type = "Test type" };

                var createCategoryDto = this.mapper.Map<CategoryDto>(category);
                var sut = new CategoryService(assertContext, mapper);
                var newCategory = await sut.CreateAsync(createCategoryDto);

                Assert.AreEqual(testCategoryId, newCategory.Id);
                Assert.AreEqual(testType, newCategory.Type);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task ThrowWhen_TypeDoesNotExist()
        {
            var categoryDto = new CategoryDto { Type = "Test type" };

            var mockCategoryService = new Mock<ICategoryService>().Object;

            var options = Utils.GetOptions(nameof(ThrowWhen_TypeDoesNotExist));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Categories.Add(new Category { Type = "Test type" });

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new CategoryService(assertContext, mapper);
                var category = await sut.CreateAsync(categoryDto);
            }
        }
    }
}