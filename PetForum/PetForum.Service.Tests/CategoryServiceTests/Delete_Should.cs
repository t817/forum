﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PetForum.Data.Database;
using PetForum.Data.Models;
using PetForum.Service;
using PetForum.Service.ModelMapper;
using PetForum.Service.Services;
using PetForum.Service.Services.Contracts;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PetForum.Tests.CategoryServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        private readonly IMapper mapper;
        public Delete_Should()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile(new AutoMapping()); });
            this.mapper = mapperConfiguration.CreateMapper();
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntityNotFound)]
        public async Task DeleteCategory_WhenExists()
        {
            int testCategoryId = 1;

            int testCategoriesCount = 1;

            var mockCategoryService = new Mock<ICategoryService>().Object;

            var options = Utils.GetOptions(nameof(DeleteCategory_WhenExists));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Categories.Add(new Category { Type = "Dogs" });

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new CategoryService(assertContext, mapper);
                var categoriesBeforeDelete = await sut.GetAllAsync();
                Assert.AreEqual(testCategoriesCount, categoriesBeforeDelete.Count());
                await sut.DeleteCategoryAsync(testCategoryId);
                var categoriesAfterDelete = await sut.GetAllAsync();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntityNotFound)]
        public async Task ThrowWhen_CategoryDoesNotExist()
        {
            int testCategoryId = 1;

            var mockCategoryService = new Mock<ICategoryService>().Object;

            var options = Utils.GetOptions(nameof(ThrowWhen_CategoryDoesNotExist));

            var config = Utils.GetMappingConfig();

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new CategoryService(assertContext, mapper);
                await sut.DeleteCategoryAsync(testCategoryId);
            }
        }
    }
}