﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PetForum.Data;
using PetForum.Data.Database;
using PetForum.Data.Models;
using PetForum.Service;
using PetForum.Service.ModelMapper;
using PetForum.Service.Services;
using PetForum.Service.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace PetForum.Tests.PostServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        private readonly IMapper mapper;
        public Delete_Should()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile(new AutoMapping()); });
            this.mapper = mapperConfiguration.CreateMapper();
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntityNotFound)]
        public async Task Throw_WhenPostIsNotMadeByUSer()
        {
            int testPostId = 1;

            var mockPostService = new Mock<IPostService>().Object;

            var options = Utils.GetOptions(nameof(Throw_WhenPostIsNotMadeByUSer));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Users.Add(new User { Username = "TestUser1", Password = "Abc23kc*r", Email = "test1@abv.bg", DisplayName = "TestU1", FirstName = "Kiril", LastName = "Petkov", Role = "Member" });
                arrangeContext.Categories.Add(new Category { Type = "Dogs" });
                arrangeContext.Posts.Add(new Post {UserId = 1, CategoryId = 1, Title = "Test title", Content = "This is a test content." });

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var user = new User { Username = "TestUser1", Password = "Abc23kc*r", Email = "test1@abv.bg", DisplayName = "TestU1", FirstName = "Kiril", LastName = "Petkov", Role = "Member" };
                var sut = new PostService(assertContext, mapper);

                await sut.DeletePostAsync(user, testPostId);

            }
        }
    }
}