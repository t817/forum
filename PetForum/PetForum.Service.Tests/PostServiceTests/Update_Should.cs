﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PetForum.Data;
using PetForum.Data.Database;
using PetForum.Service;
using PetForum.Service.DTOs;
using PetForum.Service.ModelMapper;
using PetForum.Service.Services;
using PetForum.Service.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace PetForum.Tests.PostServiceTests
{
    [TestClass]
    public class Update_Should
    {
        private readonly IMapper mapper;
        public Update_Should()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile(new AutoMapping()); });
            this.mapper = mapperConfiguration.CreateMapper();
        }

        [TestMethod]
        public async Task UpdatePost_Correctly()
        {
            int testPostId = 1;

            var mockPostService = new Mock<IPostService>().Object;

            var options = Utils.GetOptions(nameof(UpdatePost_Correctly));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Posts.Add(new Post { Title = "TestTitle", Content = "Test Content"});

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var updatePostDto = new UpdatePostDto()
                {
                    Title = "NewTitle",                    
                    Content = "This is a dog food content.",
                    ModifiedOn = DateTime.Now
                };

                var sut = new PostService(assertContext, mapper);
                var updatedPost = await sut.UpdatePostAsync(testPostId, updatePostDto);
                Assert.AreEqual("NewTitle", updatedPost.Title);
                Assert.AreEqual("This is a dog food content.", updatedPost.Content);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntityNotFound)]
        public async Task ThrowWneh_IdDoesNotExist()
        {
            int testPostId = 3;

            var mockPostService = new Mock<IPostService>().Object;

            var options = Utils.GetOptions(nameof(ThrowWneh_IdDoesNotExist));

            var config = Utils.GetMappingConfig();

            using (var assertContext = new PetForumContext(options))
            {
                var updatePostDto = new UpdatePostDto()
                {
                    Title = "NewTitle",
                    Content = "This is a dog food content.",
                    ModifiedOn = DateTime.Now
                };

                var sut = new PostService(assertContext, mapper);
                var post = await sut.UpdatePostAsync(testPostId, updatePostDto);
            }
        }
    }
}