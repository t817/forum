﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PetForum.Data;
using PetForum.Data.Database;
using PetForum.Data.Models;
using PetForum.Service;
using PetForum.Service.ModelMapper;
using PetForum.Service.Services;
using PetForum.Service.Services.Contracts;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PetForum.Tests.PostServiceTests
{
    [TestClass]
    public class Get_Should
    {
        private readonly IMapper mapper;
        public Get_Should()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile(new AutoMapping()); });
            mapper = mapperConfiguration.CreateMapper();
        }

        [TestMethod]
        public async Task GetCorrectPost_ById()
        {
            int testPostId = 1;

            var mockPostService = new Mock<IPostService>().Object;

            var options = Utils.GetOptions(nameof(GetCorrectPost_ById));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {

                arrangeContext.Users.Add(new User { Username = "TestUser1", Password = "Abc23kc*r", Email = "test1@abv.bg", DisplayName = "TestU1", FirstName = "Kiril", LastName = "Petkov", Role = "Member" });

                arrangeContext.Categories.Add(new Category { Type = "Dogs" });

                arrangeContext.Posts.Add(new Post { UserId = 1, CategoryId = 1, Content = "This is a test content." });

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new PostService(assertContext, mapper);
                var post = await sut.GetPostByIdAsync(testPostId);
                Assert.AreEqual(testPostId, post.Id);
            }
        }

        [TestMethod]
        public async Task GetCorrectPosts_ByTitle()
        {
            string testTitle = "Test title";
            int testCount = 1;

            var mockPostService = new Mock<IPostService>().Object;

            var options = Utils.GetOptions(nameof(GetCorrectPosts_ByTitle));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Users.Add(new User { Username = "TestUser1", Password = "Abc23kc*r", Email = "test1@abv.bg", DisplayName = "TestU1", FirstName = "Kiril", LastName = "Petkov", Role = "Member" });

                arrangeContext.Categories.Add(new Category { Type = "Dogs" });

                arrangeContext.Posts.Add(new Post 
                { Title = "Test title", Content = "This is a test content.", UserId = 1, CategoryId = 1 });

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new PostService(assertContext, mapper);
                var posts = await sut.GetPostByTitleAsync(testTitle);
                Assert.AreEqual(testCount, posts.Count());
            }
        }

        [TestMethod]
        public async Task GetCorrectPosts_ByContent()
        {
            string testContent = "dog food";
            int testCount = 2;

            var mockPostService = new Mock<IPostService>().Object;

            var options = Utils.GetOptions(nameof(GetCorrectPosts_ByContent));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Posts.Add(new Post { Title = "Test title1", Content = "This is a dog food." });

                arrangeContext.Posts.Add(new Post { Title = "Test title2", Content = "This is a dog food content." });

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new PostService(assertContext, mapper);
                var posts = await sut.GetPostByContentAsync(testContent);
                Assert.AreEqual(testCount, posts.Count());
            }
        }

        [TestMethod]
        public async Task GetCorrectPosts_ByUserId()
        {
            int testUserId = 1;

            int testCount = 2;

            var mockPostService = new Mock<IPostService>().Object;

            var options = Utils.GetOptions(nameof(GetCorrectPosts_ByUserId));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Users.Add(new User { Username = "TestUser1", Password = "Abc23kc*r", Email = "test1@abv.bg", DisplayName = "TestU1", FirstName = "Kiril", LastName = "Petkov", Role = "Member" });

                arrangeContext.Posts.Add(new Post 
                { UserId = 1, Title = "Test title", Content = "This is a test content." }) ;

                arrangeContext.Posts.Add(new Post 
                { UserId = 1, Title = "Another test tile", Content = "This is another test content." });

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new PostService(assertContext, mapper);
                var posts = await sut.GetUserPostsAsync(testUserId);
                Assert.AreEqual(testCount, posts.Count());
            }
        }

        [TestMethod]
        public async Task GetCorrectPost_ByCategoryId()
        {
            int testCategoryId = 1;

            int testCount = 2;

            var mockPostService = new Mock<IPostService>().Object;

            var options = Utils.GetOptions(nameof(GetCorrectPost_ByCategoryId));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Categories.Add(new Category { Type = "Dogs" });

                arrangeContext.Posts.Add(new Post { Title = "Test title", Content = "This is a test content.", CategoryId = 1 });

                arrangeContext.Posts.Add(new Post { Title = "Test title", Content = "This is a test content.", CategoryId = 1 });

                arrangeContext.Posts.Add(new Post { Title = "Test title", Content = "This is a test content.", CategoryId = 2 });

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new PostService(assertContext, mapper);
                var posts = await sut.GetPostsByCategoryAsync(testCategoryId);
                Assert.AreEqual(testCount, posts.Count());
            }
        }

        [TestMethod]
        public async Task GetAllPosts()
        {
            int testCount = 2;

            var mockPostService = new Mock<IPostService>().Object;

            var options = Utils.GetOptions(nameof(GetAllPosts));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Users.Add(new User { Username = "TestUser1", Password = "Abc23kc*r", Email = "test1@abv.bg", DisplayName = "TestU1", FirstName = "Kiril", LastName = "Petkov", Role = "Member" });

                arrangeContext.Categories.Add(new Category { Type = "Dogs" });

                arrangeContext.Posts.Add(new Post {UserId = 1, CategoryId = 1, Content = "This is a test content." });

                arrangeContext.Posts.Add(new Post {UserId = 1, CategoryId = 1, Title = "Test title", Content = "This is a test content." });

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new PostService(assertContext, mapper);
                var posts = await sut.GetAllAsync();
                Assert.AreEqual(testCount, posts.Count());
            }
        }

        [TestMethod]
        public void GetAllPostLikes()
        {
            int testCount = 1;

            var mockPostService = new Mock<IPostService>().Object;

            var options = Utils.GetOptions(nameof(GetAllPostLikes));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Posts.Add(new Post { Title = "Test title", Content = "This is a test content.", UserId = 1 });

                arrangeContext.PostLikes.Add(new PostLike { PostId = 1, UserId = 1 });

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new PostService(assertContext, mapper);
                var postLikes = sut.GetLikesCount(1);
                Assert.AreEqual(testCount, postLikes);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntityNotFound)]
        public async Task ThrowWhen_IdDoesNotExist()
        {
            int testPostId = 3;

            var mockPostService = new Mock<IPostService>().Object;

            var options = Utils.GetOptions(nameof(ThrowWhen_IdDoesNotExist));

            var config = Utils.GetMappingConfig();

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new PostService(assertContext, mapper);
                var post = await sut.GetPostByIdAsync(testPostId);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntitiesNotFound)]
        public async Task ThrowWhen_TitleDoesNotExist()
        {
            string testTitle = "Test title";

            var mockPostService = new Mock<IPostService>().Object;

            var options = Utils.GetOptions(nameof(ThrowWhen_TitleDoesNotExist));

            var config = Utils.GetMappingConfig();

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new PostService(assertContext, mapper);
                var post = await sut.GetPostByTitleAsync(testTitle);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntitiesNotFound)]
        public async Task ThrowWhen_PostsAreNotFound()
        {
            int testUserId = 2;

            var mockPostService = new Mock<IPostService>().Object;

            var options = Utils.GetOptions(nameof(ThrowWhen_PostsAreNotFound));

            var config = Utils.GetMappingConfig();

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new PostService(assertContext, mapper);
                var post = await sut.GetUserPostsAsync(testUserId);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntityNotFound)]
        public async Task ThrowWhen_CategoryDoesNotExist()
        {
            int testCategoryId = 1;

            var mockPostService = new Mock<IPostService>().Object;

            var options = Utils.GetOptions(nameof(ThrowWhen_CategoryDoesNotExist));

            var config = Utils.GetMappingConfig();

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new PostService(assertContext, mapper);
                var post = await sut.GetPostsByCategoryAsync(testCategoryId);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntitiesNotFound)]
        public async Task ThrowWhen_NoPostsFound()
        {
            var mockPostService = new Mock<IPostService>().Object;
            var mockMapper = new Mock<IMapper>().Object;
            var options = Utils.GetOptions(nameof(ThrowWhen_NoPostsFound));

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new PostService(assertContext, mockMapper);
                var posts = await sut.GetAllAsync();
            }
        }
    }
}