﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PetForum.Data;
using PetForum.Data.Database;
using PetForum.Data.Models;
using PetForum.Service.DTOs;
using PetForum.Service.ModelMapper;
using PetForum.Service.Services;
using PetForum.Service.Services.Contracts;
using System.Threading.Tasks;

namespace PetForum.Tests.PostServiceTests
{
    [TestClass]
    public class Create_Should
    {
        private readonly IMapper mapper;
        public Create_Should()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile(new AutoMapping()); });
            this.mapper = mapperConfiguration.CreateMapper();
        }

        [TestMethod]
        public async Task CreatePost()
        {
            string testTitle = "Test title1";

            string testContent = "This is a dog food.";

            int testUserId = 1;

            var mockPostService = new Mock<IPostService>().Object;

            var options = Utils.GetOptions(nameof(CreatePost));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Users.Add(new User { Username = "TestUser1", Password = "Abc23kc*r", Email = "test1@abv.bg", DisplayName = "TestU1", FirstName = "Kiril", LastName = "Petkov", Role = "Member" });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var post = new Post { Title = "Test title1", Content = "This is a dog food.", UserId = 1};

                var createPostDto = this.mapper.Map<CreatePostDto>(post);
                var sut = new PostService(assertContext, mapper);
                var newPost = await sut.CreatePostAsync(createPostDto);

                Assert.AreEqual(testUserId, newPost.UserId);
                Assert.AreEqual(testTitle, newPost.Title);
                Assert.AreEqual(testContent, newPost.Content);
            }
        }
    }
}