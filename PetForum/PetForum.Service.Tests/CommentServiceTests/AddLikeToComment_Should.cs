﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PetForum.Data.Database;
using PetForum.Data.Models;
using PetForum.Service;
using PetForum.Service.DTOs;
using PetForum.Service.ModelMapper;
using PetForum.Service.Services;
using System;
using System.Threading.Tasks;

namespace PetForum.Tests.CommentServiceTests
{
    [TestClass]
    public class AddLikeToComment_Should
    {
        private readonly IMapper mapper;
        public AddLikeToComment_Should()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile(new AutoMapping()); });
            mapper = mapperConfiguration.CreateMapper();
        }

        [TestMethod]
        public async Task AddLike_WhenCommentExists()
        {
            int testCommentId = 1;
            int testCommentLikeId = 1;

            var mockCommentService = new Mock<ICommentService>().Object;

            var options = Utils.GetOptions(nameof(AddLike_WhenCommentExists));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Comments.Add(new Comment { Content = "My dog has the same problem. I can give you some advice.", PostId = 3, UserId = 1 });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var comment = new Comment {Content = "My dog has the same problem. I can give you some advice.", PostId = 3, UserId = 1 };
                var commentLike = new CommentLike {Id = 1, UserId = 1, CommentId = 1 };
                var sut = new CommentService(assertContext, mapper);
                var commentLikeDto = this.mapper.Map<CommentLikeDto>(commentLike);
                await sut.AddLikeToComment(testCommentId, commentLikeDto);
                Assert.AreEqual(testCommentLikeId, commentLikeDto.CommentId);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntityNotFound)]
        public async Task Throw_WhenCommentDoesntExist()
        {
            int testCommentId = 1;

            var mockCommentService = new Mock<ICommentService>().Object;

            var options = Utils.GetOptions(nameof(Throw_WhenCommentDoesntExist));

            var config = Utils.GetMappingConfig();

            using (var assertContext = new PetForumContext(options))
            {
                var commentLike = new CommentLike { Id = 1, UserId = 1 };
                var sut = new CommentService(assertContext, mapper);
                var commentLikeDto = this.mapper.Map<CommentLikeDto>(commentLike);
                await sut.AddLikeToComment(testCommentId, commentLikeDto);
            }
        }
    }
}