﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PetForum.Data.Database;
using PetForum.Data.Models;
using PetForum.Service;
using PetForum.Service.ModelMapper;
using PetForum.Service.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PetForum.Tests.CommentServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        private readonly IMapper mapper;
        public Delete_Should()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile(new AutoMapping()); });
            this.mapper = mapperConfiguration.CreateMapper();
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntityNotFound)]
        public async Task DeleteComment_WhenExists()
        {
            int testCommentId = 1;
            int testCommentsCount = 1;

            var mockCommentService = new Mock<ICommentService>().Object;

            var options = Utils.GetOptions(nameof(DeleteComment_WhenExists));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Comments.Add(new Comment 
                { Content = "My dog has the same problem. I can give you some advice.", PostId = 3, UserId = 1 });
                arrangeContext.CommentLikes.Add(new CommentLike { UserId = 1, CommentId = 1 });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new CommentService(assertContext, mapper);
                var commentsBeforeDelete = await sut.GetAllCommentsAsync();
                Assert.AreEqual(testCommentsCount, commentsBeforeDelete.Count());
                await sut.DeleteCommentAsync(testCommentId);
                var commentsAfterDelete = await sut.GetAllCommentsAsync();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntityNotFound)]
        public async Task ThrowWhen_CommentDoesNotExist()
        {
            int testCommentId = 1;

            var mockUserService = new Mock<ICommentService>().Object;

            var options = Utils.GetOptions(nameof(ThrowWhen_CommentDoesNotExist));

            var config = Utils.GetMappingConfig();

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new CommentService(assertContext, mapper);
                await sut.DeleteCommentAsync(testCommentId);
            }
        }
    }
}