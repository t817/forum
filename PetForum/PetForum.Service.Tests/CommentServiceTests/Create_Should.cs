﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PetForum.Data;
using PetForum.Data.Database;
using PetForum.Data.Models;
using PetForum.Service;
using PetForum.Service.DTOs;
using PetForum.Service.ModelMapper;
using PetForum.Service.Services;
using System;
using System.Threading.Tasks;

namespace PetForum.Tests.CommentServiceTests
{
    [TestClass]
    public class Create_Should
    {
        private readonly IMapper mapper;
        public Create_Should()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile(new AutoMapping()); });
            this.mapper = mapperConfiguration.CreateMapper();
        }

        [TestMethod]
        public async Task CreateComment()
        {
            string testContent = "My dog has the same problem. I can give you some advice.";
            int testUserId = 1;
            int testPostId = 1;

            var mockCommentService = new Mock<ICommentService>().Object;

            var options = Utils.GetOptions(nameof(CreateComment));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Users.Add(new User { Username = "TestUser1", Password = "Abc23kc*r", Email = "test1@abv.bg", DisplayName = "TestU1", FirstName = "Kiril", LastName = "Petkov", Role = "Member" });

                arrangeContext.Posts.Add(new Post
                                        { Title = "Dog health", Content = "My dog has a skin problem.", UserId = 1 });

                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var comment = new Comment 
                { Content = "My dog has the same problem. I can give you some advice.", PostId = 1, UserId = 1 };

                var createCommentDto = this.mapper.Map<CreateCommentDto>(comment);   
                var sut = new CommentService(assertContext, mapper);
                var newComment = await sut.CreateCommentAsync(createCommentDto);
                Assert.AreEqual(testContent, newComment.Content);
                Assert.AreEqual(testUserId, newComment.UserId);
                Assert.AreEqual(testPostId, newComment.PostId);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntityNotFound)]
        public async Task ThrowWneh_UserIdDoesNotExist()
        {
            var mockCommentService = new Mock<ICommentService>().Object;

            var options = Utils.GetOptions(nameof(ThrowWneh_UserIdDoesNotExist));

            var config = Utils.GetMappingConfig();

            using (var assertContext = new PetForumContext(options))
            {
                var createCommentDto = new CreateCommentDto();
                var sut = new CommentService(assertContext, mapper);
                var comment = await sut.CreateCommentAsync(createCommentDto);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntityNotFound)]
        public async Task ThrowWneh_PostIdDoesNotExist()
        {
            var mockCommentService = new Mock<ICommentService>().Object;

            var options = Utils.GetOptions(nameof(ThrowWneh_PostIdDoesNotExist));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Users.Add(new User { Username = "TestUser1", Password = "Abc23kc*r", Email = "test1@abv.bg", DisplayName = "TestU1", FirstName = "Kiril", LastName = "Petkov", Role = "Member" });

                arrangeContext.SaveChanges();
            }
            using (var assertContext = new PetForumContext(options))
            {
                var comment = new Comment 
                { Content = "My dog has the same problem. I can give you some advice.", PostId = 3, UserId = 1 };

                var createCommentDto = this.mapper.Map<CreateCommentDto>(comment);
                var sut = new CommentService(assertContext, mapper);
                var newComment = await sut.CreateCommentAsync(createCommentDto);
            }
        }
    }
}