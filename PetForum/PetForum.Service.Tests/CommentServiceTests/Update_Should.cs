﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PetForum.Data.Database;
using PetForum.Data.Models;
using PetForum.Service;
using PetForum.Service.DTOs.Comment;
using PetForum.Service.ModelMapper;
using PetForum.Service.Services;
using System;
using System.Threading.Tasks;

namespace PetForum.Tests.CommentServiceTests
{
    [TestClass]
    public class Update_Should
    {
        private readonly IMapper mapper;
        public Update_Should()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile(new AutoMapping()); });
            this.mapper = mapperConfiguration.CreateMapper();
        }

        [TestMethod]
        public async Task UpdateUser_Correctly()
        {
            int testCommentId = 1;

            var mockCommentService = new Mock<ICommentService>().Object;

            var options = Utils.GetOptions(nameof(UpdateUser_Correctly));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Comments.Add(new Comment 
                { Content = "My dog has the same problem. I can give you some advice.", PostId = 3, UserId = 1 });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var updateCommentDto = new UpdateCommentDto()
                {
                    Content = "My dog has the same problem. I can give you phone of the veterinarian.",
                    ModifiedOn = DateTime.Now
                };

                var sut = new CommentService(assertContext, mapper);
                var updatedComment = await sut.UpdateCommentAsync(testCommentId, updateCommentDto);

                Assert.AreEqual("My dog has the same problem. I can give you phone of the veterinarian.", updatedComment.Content);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntityNotFound)]
        public async Task ThrowWneh_CommentDoesNotExist()
        {
            int testCommentId = 3;

            var mockICommentService = new Mock<ICommentService>().Object;

            var options = Utils.GetOptions(nameof(ThrowWneh_CommentDoesNotExist));

            var config = Utils.GetMappingConfig();

            using (var assertContext = new PetForumContext(options))
            {
                var updateCommentDto = new UpdateCommentDto()
                {
                    Content = "My dog has the same problem. I can give you phone of the veterinarian.",
                    ModifiedOn = DateTime.Now
                };

                var sut = new CommentService(assertContext, mapper);
                var updatedComment = await sut.UpdateCommentAsync(testCommentId, updateCommentDto);
            }
        }
    }
}