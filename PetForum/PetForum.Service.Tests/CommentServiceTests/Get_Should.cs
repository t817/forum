﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PetForum.Data.Database;
using PetForum.Data.Models;
using PetForum.Service;
using PetForum.Service.ModelMapper;
using PetForum.Service.Services;
using System;
using System.Threading.Tasks;

namespace PetForum.Tests.CommentServiceTests
{
    [TestClass]
    public class Get_Should
    {
        private readonly IMapper mapper;
        public Get_Should()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile(new AutoMapping()); });
            mapper = mapperConfiguration.CreateMapper();
        }

        [TestMethod]
        public async Task GetCorrectComment_ById()
        {
            int testCommentId = 1;
            string testCommentContent = "My dog has the same problem. I can give you some advice.";

            var mockCommentService = new Mock<ICommentService>().Object;

            var options = Utils.GetOptions(nameof(GetCorrectComment_ById));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Comments.Add(new Comment 
                { Content = "My dog has the same problem. I can give you some advice.", PostId = 3, UserId = 1 });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new CommentService(assertContext, mapper);
                var comment = await sut.GetCommentByIdAsync(testCommentId);
                Assert.AreEqual(testCommentContent, comment.Content);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntityNotFound)]
        public async Task ThrowWneh_IdDoesNotExist()
        {
            int testCommentId = 3;

            var mockCommentService = new Mock<ICommentService>().Object;

            var options = Utils.GetOptions(nameof(ThrowWneh_IdDoesNotExist));

            var config = Utils.GetMappingConfig();

            using (var assertContext = new PetForumContext(options))
            {
                var sut = new CommentService(assertContext, mapper);
                var comment = await sut.GetCommentByIdAsync(testCommentId);
            }
        }
    }
}