﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PetForum.Data.Database;
using PetForum.Data.Models;
using PetForum.Service;
using PetForum.Service.DTOs;
using PetForum.Service.ModelMapper;
using PetForum.Service.Services;
using PetForum.Service.Services.Contracts;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PetForum.Tests.NotificationServiceTests
{
    [TestClass]
    public class Create_Should
    {
        private readonly IMapper mapper;
        public Create_Should()
        {
            var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile(new AutoMapping()); });
            this.mapper = mapperConfiguration.CreateMapper();
        }

        [TestMethod]
        public async Task CreateNotification()
        {
            int testNotificationCount = 1;

            var mockNotificationService = new Mock<INotificationService>().Object;

            var options = Utils.GetOptions(nameof(CreateNotification));

            var config = Utils.GetMappingConfig();

            using (var arrangeContext = new PetForumContext(options))
            {
                arrangeContext.Users.Add(new User { Username = "TestUser1", Password = "Abc23kc*r", Email = "test1@abv.bg", DisplayName = "TestU1", FirstName = "Kiril", LastName = "Petkov", Role = "Member" });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new PetForumContext(options))
            {
                var notification = new Notification { Text = "Test text", UserId = 1 };

                var notificationDto = this.mapper.Map<NotificationDto>(notification);
                var sut = new NotificationService(assertContext, mapper);
                var newNotification = await sut.CreateNotificationAsync(notificationDto);

                Assert.AreEqual(testNotificationCount, assertContext.Notifications.Count());
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException), Exceptions.EntityNotFound)]
        public async Task ThrowWhen_NotificationDoesNotExist()
        {
            int testCategoryId = 1;

            var mockNotificationService = new Mock<INotificationService>().Object;

            var options = Utils.GetOptions(nameof(ThrowWhen_NotificationDoesNotExist));

            var config = Utils.GetMappingConfig();

            using (var assertContext = new PetForumContext(options))
            {
                var notification = new Notification { Text = "Test text", UserId = 1 };

                var notificationDto = this.mapper.Map<NotificationDto>(notification);
                var sut = new NotificationService(assertContext, mapper);

                var newNotification = await sut.CreateNotificationAsync(notificationDto);
            }
        }
    }
}