﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PetForum.Data.Database;
using PetForum.Data.Models;
using PetForum.Service.DTOs.User;

namespace PetForum.Tests
{
    public class Utils
    {
        public static DbContextOptions<PetForumContext> GetOptions(string database)
        {
            return new DbContextOptionsBuilder<PetForumContext>()
                       .UseInMemoryDatabase(database)
                       .Options;
        }

        public static MapperConfiguration GetMappingConfig()
        {
            return new MapperConfiguration(opts =>
            {
                opts.CreateMap<User, GetUserDto>();
            });
        }
    }
}