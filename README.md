<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

# PetForum

## Project Description
Forum is an online discussion site where people can hold conversations by posting, commenting and liking messages about their pets. 

## Functional Requirements 
### Entities

-	Each user must have a username, password, email, and display name. 

- [ ] Username must be unique and between 2 and 20 symbols.
- [ ] Password must be at least 8 symbols and should contain capital letter, digit and special symbol (+, -, *, &, ^, …)
- [ ] Display name must be between 2 and 20 symbols.
- [ ] Email must be valid email and unique in the system.
-   Each post must have a title, content, comments and likes.

### Public Part
The public part must be accessible without authentication.
- On the home page anonymous users must be presented with the core features of the platform as well as how many people are using it and how many posts have been created so far. 
- Anonymous users must be able to register and login. Registration of a new user should use email verification flow.
- Anonymous users should be able to see a list of the top 10 most commented posts and a list of the 10 most recently created posts.

### Private Part 
Accessible only if the user is authenticated.
- User must be able to login and logout.
- User must be able to browse posts created by the other users with an option to sort and filter them. The list of posts should support pagination.
- User must be able to view a single post including its title, content, comments, likes etc. The details of the post and any available user actions (comment/like/edit) should be presented on a separate page. 
- User must be able to update their profile information including setting a profile picture. Users should not be able to change their username once registered.
- User must be able to create a new post with at least a title and content.
- Each user must be able to edit only personal posts or comments. 
- Each user must be able to view all his posts (with option to filter and sort them), all his feedback and all feedback for any other user. List with travels/feedback should support pagination.
- Each user must be able to remove one or more of their own posts. Deleting a post should be available while reading the details of an individual post or when browsing the list of all posts.

### Administrative part
Accessible to users with administrative privileges.
- Admin must be able to search for a user by their username, email or display name. List with users should support pagination.
- Admin must be able to block or unblock individual users. A blocked user must not be able to create posts or comments. 
- Admin must be able to view a list of all posts with an option to filter and sort them. List with posts should support pagination.

# REST API  
To provide other developers with your service, you need to develop a REST API. It should leverage HTTP as a transport protocol and clear text JSON for the request and response payloads.
A great API is nothing without a great documentation. The documentation holds the information that is required to successfully consume and integrate with an API. You must use Swagger to document yours.

### The REST API provides the following capabilities:  
1.	Users  
    · CRUD operations (must)  
    · Block/unblock user (must)  
    · Search by username, email, or display name (must)  
2.	Posts  
    · CRUD operations (must)  
    · Comment (must)  
    · List and manage (comment/like) posts (must)  
    · Filter and sort posts (must)  
 

#### Link to the Swagger documentation: http://localhost:34021/swagger/index.html
## Members: Keti, Rosen, Sonya



